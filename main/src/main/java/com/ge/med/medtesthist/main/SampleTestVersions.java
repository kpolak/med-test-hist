package com.ge.med.medtesthist.main;

import com.ge.med.medtesthist.dao.TestVersionDAO;
import com.ge.med.medtesthist.dao.exceptions.DBModificationException;
import com.ge.med.medtesthist.dao.exceptions.VersionDoesNotExistException;

import javax.persistence.EntityManager;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * TO BE DOCUMENTED
 * User: Krzysztof Polak
 * Date: 05/12/13
 * Time: 14:06
 */
public class SampleTestVersions {

  private static Map<String, String> sampleTestVersions = new LinkedHashMap<>();

  static {
    sampleTestVersions.put("RIS_5.0.7", "");
    sampleTestVersions.put("RIS_5.0.7.1", "RIS_5.0.7");
    sampleTestVersions.put("RIS_5.0.7.2", "RIS_5.0.7");
    sampleTestVersions.put("RIS_5.0.7.3", "RIS_5.0.7");
    sampleTestVersions.put("RIS_5.0.8", "");
    sampleTestVersions.put("RIS_5.0.8.1", "RIS_5.0.8");
    sampleTestVersions.put("RIS_5.0.8.2", "RIS_5.0.8");
    sampleTestVersions.put("RIS_5.0.8.3", "RIS_5.0.8");
    sampleTestVersions.put("RIS_5.0.8.4", "RIS_5.0.8");
    sampleTestVersions.put("RIS_5.0.8.5", "RIS_5.0.8");
    sampleTestVersions.put("RIS_5.0.8.6", "RIS_5.0.8");
    sampleTestVersions.put("RIS_5.0.8.7", "RIS_5.0.8");
  }

  public static void main(String[] args) {
  }

  public void createSampleVersions(EntityManager em) throws VersionDoesNotExistException, DBModificationException {

    TestVersionDAO testVersionDAO = new TestVersionDAO();
    testVersionDAO.setEm(em);
    testVersionDAO.createVersionData(sampleTestVersions, false);

  }


}
