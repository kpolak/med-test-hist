package com.ge.med.medtesthist.main;


import com.ge.med.medtesthist.dao.exceptions.DBModificationException;
import com.ge.med.medtesthist.dao.exceptions.VersionDoesNotExistException;
import com.ge.med.medtesthist.model.views.TestVersionDTO;
import com.ge.med.medtesthist.reader.ReadingResults;
import com.ge.med.medtesthist.services.ProtocolReaderService;
import com.ge.med.medtesthist.services.ProtocolsService;
import lombok.extern.java.Log;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;


//@Log
public class Main {

  public static final String RIS508_SC_REGRESSION_P3_HIGH_VTP_XLS = "../RIS508-SC_Regression_P3_HIGH_VTP.xls";
  public static final String RIS_5083 = "RIS_5.0.8.3";
  public static final String MEDTEST_PU = "medtestPU";

  public static void main(String[] args) throws VersionDoesNotExistException, DBModificationException {

    EntityManagerFactory emf = Persistence.createEntityManagerFactory(MEDTEST_PU);
    EntityManager em = emf.createEntityManager();

    createSampleData(em);
//    readSampleSpreadsheet(em);
//    readSampleDirectoryWithSpreadsheets(em);

    em.close();
    emf.close();
  }

  private static void getTestVersionsWithProtocols() {
    ProtocolsService protocolsService = new ProtocolsService();
    List<TestVersionDTO> testVersions = protocolsService.getAllTestVersionsWithProtocols();
    for (TestVersionDTO tv : testVersions) {
      //     log.info("Test version: " + tv.toString());
    }
  }

  public static void readSampleDirectoryWithSpreadsheets(EntityManager em) {
    ProtocolReaderService protocolReaderService = new ProtocolReaderService();
    //List<Protocol> protocolList = protocolReaderService.readProtocolsFromDirectory("/Users/kpolak/src/myApps/medtesthist/examples/RIS508/TR3/", false, RIS_5083);
    List<ReadingResults> protocolList = protocolReaderService.readProtocolsFromDirectory("d:\\ge\\tests\\RIS508\\TR3",
        RIS_5083, false);
    for (ReadingResults rr : protocolList) {
      //    log.info("Protocol: " + rr.getProtocol().getFileName() + ", TCs: " + rr.getProtocol().getNumberOfTestCases());
    }

  }
//
//  private static void readSampleSpreadsheet(EntityManager em) {
//    ProtocolReaderService protocolReaderService = new ProtocolReaderService();
//    ReadingResults rr = protocolReaderService.readSingleProtocol(RIS508_SC_REGRESSION_P3_HIGH_VTP_XLS, RIS_5083, false);
//    log.info(rr.getProtocol().toString());
//  }

  private static void createSampleData(EntityManager em) throws VersionDoesNotExistException, DBModificationException {
    SampleTestVersions sampleTesVersions = new SampleTestVersions();
    sampleTesVersions.createSampleVersions(em);
  }
}