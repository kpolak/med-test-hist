package com.ge.med.medtesthist.dao.utils;

import com.ge.med.medtesthist.model.TestCase;

import java.util.Random;

/**
 * TO BE DOCUMENTED
 * User: Krzysztof Polak
 * Date: 28/01/14
 * Time: 14:38
 */
public class TestCasesBuilder {

  static Random randomGenerator = new Random();

  public final static String[] DEFAULT_TEST_CASES = {"DEF_10", "DEF_11", "DEF_12", "DEF_13", "DEF_14", "DEF_15", "DEF_16", "DEF_17", "DEF_18", "DEF_19", "DEF_20"};

  public static TestCase createRandomTestCase() {
    int randomInt = randomGenerator.nextInt(1000);
    return createTestCase("RAND_" + randomInt);
  }

  public static TestCase[] createDefaultSetTestCases() {
    TestCase[] testCases = new TestCase[DEFAULT_TEST_CASES.length];
    for (int i = 0; i < DEFAULT_TEST_CASES.length; i++) {
      TestCase testCase = createTestCase(DEFAULT_TEST_CASES[i]);
      testCase.setPrecondition(testCase.getPrecondition() + " " + i);
      testCase.setTestInput(testCase.getTestInput() + " " + i);
      testCase.setTestOutput(testCase.getTestOutput() + " " + i);
      testCase.setUserNote(testCase.getUserNote() + " " + i);
      testCases[i] = testCase;
    }
    return testCases;
  }

  public static TestCase createTestCase(String testCode) {
    TestCase testCase = new TestCase();
    testCase.setTestCode(testCode);
    testCase.setPrecondition("Precondition");
    testCase.setTestInput("Test input");
    testCase.setTestOutput("Test output");
    testCase.setUserNote("User note");

    return testCase;
  }
}
