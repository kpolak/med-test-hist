package com.ge.med.medtesthist.dao;

import com.ge.med.medtesthist.dao.exceptions.DBModificationException;
import com.ge.med.medtesthist.dao.exceptions.TestCaseCannotBeCreatedException;
import com.ge.med.medtesthist.dao.exceptions.TestCaseDoesNotExistException;
import com.ge.med.medtesthist.dao.utils.TestCasesBuilder;
import com.ge.med.medtesthist.model.TestCase;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.Random;

import static com.ge.med.medtesthist.dao.utils.TestCasesBuilder.DEFAULT_TEST_CASES;
import static org.testng.AssertJUnit.*;

/**
 * TestCaseDAO testing class.
 * User: Krzysztof Polak
 * Date: 28/01/14
 * Time: 14:29
 */
public class TestCaseDAOTestNg {

  static EntityManagerFactory emf;
  static EntityManager em;

  @BeforeClass
  public void setUp() {
    emf = Persistence.createEntityManagerFactory("medtestPU");
    em = emf.createEntityManager();

  }

  private TestCaseDAO buildTestCaseDAO() {
    TestCaseDAO testCaseDAO = new TestCaseDAO();
    testCaseDAO.setEm(em);
    return testCaseDAO;
  }

  ////////////// default data ///////////////////////
//  @Test(priority = 1, enabled = true)
  public void def01_createDefaultTestCases() throws TestCaseCannotBeCreatedException, DBModificationException {
    TestCaseDAO testCaseDAO = buildTestCaseDAO();

    TestCase[] testCases = TestCasesBuilder.createDefaultSetTestCases();
    for (TestCase testCase : testCases) {
      try {
        testCaseDAO.removeTestCase(testCase.getTestCode());
      } catch (TestCaseDoesNotExistException e) {
        // no problem
      }
      testCaseDAO.storeTestCase(testCase);
    }
  }


  ////////////// tests ///////////////////////

  //  @Test(priority = 10, enabled = true)
  public void test01_findTestCase() {
    TestCaseDAO testCaseDAO = buildTestCaseDAO();

    Random random = new Random();
    String randomTestCode = DEFAULT_TEST_CASES[random.nextInt(DEFAULT_TEST_CASES.length)];
    TestCase testCase = testCaseDAO.findTestCase(randomTestCode);

    assertNotNull(testCase);
    assertEquals(testCase.getTestCode(), randomTestCode);
    assertNotNull(testCase.getPrecondition());
    assertNotNull(testCase.getTestInput());
    assertNotNull(testCase.getTestOutput());
  }

  //  @Test(priority = 11, enabled = true,
//        expectedExceptions = TestCaseCannotBeCreatedException.class)
  public void def01_createDuplicateTestCases() throws TestCaseCannotBeCreatedException, DBModificationException {
    TestCaseDAO testCaseDAO = buildTestCaseDAO();

    Random random = new Random();
    String randomTestCode = DEFAULT_TEST_CASES[random.nextInt(DEFAULT_TEST_CASES.length)];
    TestCase testCase = testCaseDAO.findTestCase(randomTestCode);

    testCaseDAO.storeTestCase(testCase);
  }

  //  @Test(priority = 12, enabled = true)
  public void test01_storeAndFindTestCase()
      throws TestCaseCannotBeCreatedException, DBModificationException, TestCaseDoesNotExistException {
    TestCaseDAO testCaseDAO = buildTestCaseDAO();

    TestCase testCase = TestCasesBuilder.createRandomTestCase();
    testCaseDAO.storeTestCase(testCase);

    TestCase foundTestCase = testCaseDAO.findTestCase(testCase.getTestCode());
    assertNotNull(foundTestCase);
    assertEquals(foundTestCase.getTestCode(), testCase.getTestCode());
    assertEquals(foundTestCase.getPrecondition(), testCase.getPrecondition());
    assertEquals(foundTestCase.getTestInput(), testCase.getTestInput());
    assertEquals(foundTestCase.getTestOutput(), testCase.getTestOutput());

    assertEquals(foundTestCase.getUserNote(), testCase.getUserNote());

    testCaseDAO.removeTestCase(testCase.getTestCode());

  }


}
