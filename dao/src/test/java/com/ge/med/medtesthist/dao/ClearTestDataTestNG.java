package com.ge.med.medtesthist.dao;

import com.ge.med.medtesthist.model.Protocol;
import org.testng.annotations.BeforeClass;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

/**
 * TO BE DOCUMENTED
 * User: Krzysztof Polak
 * Date: 05/02/14
 * Time: 16:40
 */
public class ClearTestDataTestNG {

  public static final String RIS_5_0_7 = "RIS_5.0.7";
  public static final String RIS_0_0_1 = "RIS_0_0_1";
  static EntityManagerFactory emf;
  static EntityManager em;

  private static Protocol testProtocol;

  @BeforeClass
  public void setUp() {
    emf = Persistence.createEntityManagerFactory("medtestPU");
    em = emf.createEntityManager();
  }

  private ProtocolDAO buildProtocolDAO() {
    ProtocolDAO protocolDAO = new ProtocolDAO();
    TestCaseDAO testCaseDAO = new TestCaseDAO();
    testCaseDAO.setEm(em);
    protocolDAO.setTestCaseDAO(testCaseDAO);
    TestVersionDAO testVersionDAO = new TestVersionDAO();
    testVersionDAO.setEm(em);
    protocolDAO.setTestVersionDAO(testVersionDAO);
    protocolDAO.setEm(em);
    return protocolDAO;
  }

  //  @Test(priority = 1, enabled = true)
  public void clearProtocolTestCases() {
    // clear all protocol test cases
    EntityTransaction tx = em.getTransaction();
    tx.begin();
    em.createQuery("DELETE FROM ProtocolTestCase ptc WHERE ptc.testCase.testCode like :testCodeLike").
        setParameter("testCodeLike", "DEF_%").executeUpdate();
    tx.commit();
  }

  //  @Test(priority = 2, enabled = true)
  public void clearProtocols() {
    // clear all protocols
    EntityTransaction tx = em.getTransaction();
    tx.begin();
    em.createQuery("DELETE FROM Protocol p WHERE p.fileName like :fileName").
        setParameter("fileName", "Test protocol%").executeUpdate();
    tx.commit();
  }

  //  @Test(priority = 3, enabled = false)
  public void clearTestCases() {
    // clear all test cases
    EntityTransaction tx = em.getTransaction();
    tx.begin();
    em.createQuery("DELETE FROM TestCase tc WHERE tc.testCode like :testCode").
        setParameter("testCode", "DEF%").executeUpdate();
    tx.commit();
  }
}
