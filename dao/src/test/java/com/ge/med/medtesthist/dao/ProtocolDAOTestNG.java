package com.ge.med.medtesthist.dao;

import com.ge.med.medtesthist.dao.exceptions.DBModificationException;
import com.ge.med.medtesthist.dao.exceptions.ProtocolAlreadyExistsException;
import com.ge.med.medtesthist.dao.utils.TestProtocolsBuilder;
import com.ge.med.medtesthist.model.Protocol;
import org.testng.annotations.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import static org.testng.AssertJUnit.*;

/**
 * TO BE DOCUMENTED
 * User: Krzysztof Polak
 * Date: 27/01/14
 * Time: 12:43
 */

public class ProtocolDAOTestNG {
  public static final String RIS_5_0_7 = "RIS_5.0.7";
  public static final String RIS_0_0_1 = "RIS_0_0_1";
  static EntityManagerFactory emf;
  static EntityManager em;

  private static Protocol testProtocol;

  @BeforeClass
  public void setUp() {
    emf = Persistence.createEntityManagerFactory("medtestPU");
    em = emf.createEntityManager();
  }

  private ProtocolDAO buildProtocolDAO() {
    ProtocolDAO protocolDAO = new ProtocolDAO();
    TestCaseDAO testCaseDAO = new TestCaseDAO();
    testCaseDAO.setEm(em);
    protocolDAO.setTestCaseDAO(testCaseDAO);
    TestVersionDAO testVersionDAO = new TestVersionDAO();
    testVersionDAO.setEm(em);
    protocolDAO.setTestVersionDAO(testVersionDAO);
    protocolDAO.setEm(em);
    return protocolDAO;
  }

  ////////////// tests ///////////////////////
  // TODO count test cases in this version before adding new protocol and then compare


  //  @Test(priority = 1, enabled = true)
  public void test01_storeProtocol() throws DBModificationException, ProtocolAlreadyExistsException {
    ProtocolDAO protocolDAO = buildProtocolDAO();

    // delete test protocol if

    testProtocol = TestProtocolsBuilder.createTestProtocol();
    protocolDAO.updateTestVersion(testProtocol, RIS_5_0_7);

    protocolDAO.storeProtocol(testProtocol, false, true);
  }

  //  @Test(priority = 2, dependsOnMethods = "test01_storeProtocol", enabled = true)
  public void test02_findProtocol() {
    ProtocolDAO protocolDAO = buildProtocolDAO();
    Protocol storedProtocol = protocolDAO.findProtocolByNameAndVersion(TestProtocolsBuilder.TEST_PROTOCOL_FILENAME,
        RIS_5_0_7);
//    assertNotNull(storedProtocol);
//    assertEquals(testProtocol.getExecutionDate(), storedProtocol.getExecutionDate());
//    assertEquals(testProtocol.getFileName(), storedProtocol.getFileName());
//    assertEquals(testProtocol.getGuiLanguage(), storedProtocol.getGuiLanguage());
//    assertEquals(testProtocol.getTester(), storedProtocol.getTester());
//    assertEquals(testProtocol.getImportDate(), storedProtocol.getImportDate());
//    assertEquals(testProtocol.getNumberOfTestCases(), storedProtocol.getNumberOfTestCases());
//    assertEquals(testProtocol.getTestsEmptyNbr(), storedProtocol.getTestsEmptyNbr());
//    assertEquals(testProtocol.getTestsFailedNbr(), storedProtocol.getTestsFailedNbr());
//    assertEquals(testProtocol.getTestsNbr(), storedProtocol.getTestsNbr());
//    assertEquals(testProtocol.getTestsPassedNbr(), storedProtocol.getTestsPassedNbr());
//    assertEquals(testProtocol.getTestsSkippedNbr(), storedProtocol.getTestsSkippedNbr());
  }


  @Test(priority = 3, dependsOnMethods = "test01_storeProtocol", enabled = true)
  public void test03_overwriteProtocol() throws DBModificationException, ProtocolAlreadyExistsException {
    ProtocolDAO protocolDAO = buildProtocolDAO();

    testProtocol = TestProtocolsBuilder.createTestProtocol();
    protocolDAO.updateTestVersion(testProtocol, RIS_5_0_7);
    final String new_tester = "New_tester";
    testProtocol.setTester(new_tester);

    protocolDAO.storeProtocol(testProtocol, true, true);
    Protocol storedProtocol = protocolDAO.findProtocolByNameAndVersion(TestProtocolsBuilder.TEST_PROTOCOL_FILENAME,
        RIS_5_0_7);
    assertNotNull(storedProtocol);
//    assertEquals(testProtocol.getExecutionDate(), storedProtocol.getExecutionDate());
//    assertEquals(testProtocol.getFileName(), storedProtocol.getFileName());
//    assertEquals(testProtocol.getTester(), new_tester);
  }

  //  @Test(priority = 4, dependsOnMethods = "test01_storeProtocol", enabled = true,
//        expectedExceptions = ProtocolAlreadyExistsException.class)
  public void test04_notOverwriteProtocol() throws DBModificationException, ProtocolAlreadyExistsException {
    ProtocolDAO protocolDAO = buildProtocolDAO();

    testProtocol = TestProtocolsBuilder.createTestProtocol();
    protocolDAO.updateTestVersion(testProtocol, RIS_5_0_7);

    protocolDAO.storeProtocol(testProtocol, false, true);
  }


  //  @Test(priority = 5, dependsOnMethods = "test01_storeProtocol", enabled = false)
  public void test05_updateTestCasesDefinition() {
    // TODO to implement
  }

  //  @Test(priority = 6, dependsOnMethods = "test01_storeProtocol", enabled = false)
  public void test06_getProtocolsCountForTestVersion() {
    // TODO to implement
  }

  //  @Test(priority = 7, dependsOnMethods = "test01_storeProtocol", enabled = true)
  public void test07_removeProtocol() {
    ProtocolDAO protocolDAO = buildProtocolDAO();
    Protocol storedProtocol = protocolDAO.findProtocolByNameAndVersion(TestProtocolsBuilder.TEST_PROTOCOL_FILENAME,
        RIS_5_0_7);
    assertNotNull(storedProtocol);
    protocolDAO.removeProtocol(storedProtocol.getId());

    // try to find deleted protocol
    Protocol deletedProtocol = protocolDAO.findProtocolByNameAndVersion(TestProtocolsBuilder.TEST_PROTOCOL_FILENAME,
        RIS_5_0_7);
    assertNull(deletedProtocol);
  }

  //  @Test(priority = 8, dependsOnMethods = "test01_storeProtocol", enabled = false)
  public void test08_getProtocolsCountForTestVersion() {
  }


  //  @Test(priority = 101, enabled = false)
  public void test04_getTestVersionsWithProtocols() {
  }
}
