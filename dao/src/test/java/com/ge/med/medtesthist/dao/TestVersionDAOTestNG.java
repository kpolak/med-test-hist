package com.ge.med.medtesthist.dao;

import com.ge.med.medtesthist.dao.exceptions.DBModificationException;
import com.ge.med.medtesthist.dao.exceptions.VersionDoesNotExistException;
import com.ge.med.medtesthist.model.TestVersion;
import junitx.framework.ListAssert;
import org.testng.annotations.*;


import static org.testng.AssertJUnit.*;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

/**
 * TestVersionDAO test class.
 * User: Krzysztof Polak
 * Date: 27/01/14
 * Time: 10:24
 */
public class TestVersionDAOTestNG {

  public static final String RIS_5_0_7 = "RIS_5.0.7";
  public static final String RIS_0_0_1 = "RIS_0_0_1";
  static EntityManagerFactory emf;
  static EntityManager em;

  @BeforeClass
  public void setUp() {
    emf = Persistence.createEntityManagerFactory("medtestPU");
    em = emf.createEntityManager();
  }

  private TestVersionDAO buildTestVersionDAO() {
    TestVersionDAO testVersionDAO = new TestVersionDAO();
    testVersionDAO.setEm(em);
    return testVersionDAO;
  }

  ////////////// tests ///////////////////////
//  @Test(priority = 1, enabled = true)
  public void test01_createNewVersion() throws DBModificationException {
    // given
    TestVersionDAO testVersionDAO = buildTestVersionDAO();

    // when
    testVersionDAO.createNewVersion(RIS_0_0_1, RIS_5_0_7);
  }

  //  @Test(dependsOnMethods = "test01_createNewVersion", priority = 2, enabled = true)
  public void test02_findNewCreatedTestVersion() {
    // given
    TestVersionDAO testVersionDAO = buildTestVersionDAO();

    // when
    TestVersion testVersion = testVersionDAO.findTestVersion(RIS_0_0_1);

    // then
    assertNotNull(testVersion);
    assertEquals(testVersion.getVersionName(), RIS_0_0_1);
    assertNotNull(testVersion.getParentTestVersion());
    assertEquals(testVersion.getProtocolsCount(), 0);
  }

  //  @Test(dependsOnMethods = "test01_createNewVersion", priority = 3,
//        expectedExceptions = DBModificationException.class)
  public void test03_reCreateNewVersion() throws DBModificationException {
    // given
    TestVersionDAO testVersionDAO = buildTestVersionDAO();

    // when
    testVersionDAO.createNewVersion(RIS_0_0_1, RIS_5_0_7);

    // then DBModificationException should be thrown
  }

  //  @Test(dependsOnMethods = "test01_createNewVersion", priority = 4, enabled = true)
  public void test04_removeVersion() throws VersionDoesNotExistException, DBModificationException {
    // given
    TestVersionDAO testVersionDAO = buildTestVersionDAO();

    // when
    testVersionDAO.removeVersion(RIS_0_0_1);
  }

  //  @Test(dependsOnMethods = "test01_createNewVersion", priority = 5)
  public void test05_findRemovedTestVersion() {
    // given
    TestVersionDAO testVersionDAO = buildTestVersionDAO();

    // when
    TestVersion testVersion = testVersionDAO.findTestVersion(RIS_0_0_1);

    // then
    assertNull(testVersion);
  }

  //////////////////////////////////////////////
  // not depended test
  /////////////////////////////////////////////

  //  @Test(priority = 101)
  public void test11_getTestVersionsForImportingProtocols() {
    // given
    TestVersionDAO testVersionDAO = buildTestVersionDAO();

    // when
    List<TestVersion> testVersionList = testVersionDAO.getTestVersionsForImportingProtocols();
    List<TestVersion> testAllList = testVersionDAO.getAllTestVersions();

    // testVersionList.stream()
    //    .forEach(p -> System.out.println(p.getVersionName()));

    // then
    for (TestVersion testVersion : testVersionList) {
      assertFalse("Test import version contains root version", testVersion.getVersionName().equals(RIS_5_0_7));
    }
    TestVersion tv = new TestVersion(RIS_5_0_7, null);
    ListAssert.assertContains("", testAllList, tv);
  }

  //  @Test(priority = 102)
  public void test12_findTestVersion() {
    // given
    TestVersionDAO testVersionDAO = buildTestVersionDAO();

    // when
    TestVersion testVersion = testVersionDAO.findTestVersion(RIS_5_0_7);

    // then
    assertNotNull(testVersion);
    assertEquals(testVersion.getVersionName(), RIS_5_0_7);
    assertNull(testVersion.getParentTestVersion());
    assertEquals(testVersion.getProtocolsCount(), 0);
  }


}
