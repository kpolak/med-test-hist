package com.ge.med.medtesthist.dao.utils;

import com.ge.med.medtesthist.model.Protocol;
import com.ge.med.medtesthist.model.ProtocolTestCase;
import com.ge.med.medtesthist.model.TestCase;
import com.ge.med.medtesthist.model.TestVersion;
import org.joda.time.DateTime;

/**
 * TO BE DOCUMENTED
 * User: Krzysztof Polak
 * Date: 27/01/14
 * Time: 12:58
 */
public class TestProtocolsBuilder {

  public final static String TEST_PROTOCOL_FILENAME = "Test protocol X";
  public static final String RIS_5_0_7 = "RIS_5.0.7";

  public static Protocol createTestProtocol() {
    Protocol p = new Protocol();

    p.setFileName(TEST_PROTOCOL_FILENAME);
    p.setGuiLanguage("EN_GB");
    p.setExecutionDate(DateTime.now());
    p.setImportDate(DateTime.now());
    p.setTestsNbr(3);
    p.setTestsPassedNbr(3);
    p.setTestsSkippedNbr(0);
    p.setTestsFailedNbr(0);
    p.setTestsEmptyNbr(0);
    p.setTester("Krzysztof Polak");

    p.getTestCases().add(createProtocolTestCase(p, TestCasesBuilder.DEFAULT_TEST_CASES[0]));
    p.getTestCases().add(createProtocolTestCase(p, TestCasesBuilder.DEFAULT_TEST_CASES[1]));
    p.getTestCases().add(createProtocolTestCase(p, TestCasesBuilder.DEFAULT_TEST_CASES[2]));

    return p;
  }

  private static ProtocolTestCase createProtocolTestCase(Protocol p, String testCode) {
    ProtocolTestCase ptc = new ProtocolTestCase();
    ptc.setRowNbr(1);
    ptc.setTester("Krzysztof Polak");
    ptc.setTestCase(null);
    ptc.setProtocol(p);
    ptc.setTestResult(ProtocolTestCase.TestResultEnum.PASSED);
    ptc.setExecutionDate(DateTime.now());
    ptc.setObservedResults("Observed results");
    ptc.setProblemDescription("Problem description");
    ptc.setSkipRationale("Skip rationale");
    ptc.setSprFlag(false);
    ptc.setSprImpact("Impact");
    ptc.setSprNumber("SPR-001");
    ptc.setTestCode(testCode);

    TestCase testCase = TestCasesBuilder.createTestCase(testCode);
    ptc.setTestCase(testCase);

    return ptc;
  }

  private static TestVersion createProtocolTestVersion() {
    TestVersion testVersion = new TestVersion();
    testVersion.setVersionName(RIS_5_0_7);
    return testVersion;
  }


}
