package com.ge.med.medtesthist.dao.exceptions;

/**
 * TO BE DOCUMENTED
 * User: Krzysztof Polak
 * Date: 28/01/14
 * Time: 14:34
 */
public class TestCaseCannotBeCreatedException extends Exception {
  public TestCaseCannotBeCreatedException(String msg) {
    super(msg);
  }
}
