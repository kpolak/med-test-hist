package com.ge.med.medtesthist.dao;

import com.ge.med.medtesthist.dao.exceptions.DBModificationException;
import com.ge.med.medtesthist.dao.exceptions.TestCaseCannotBeCreatedException;
import com.ge.med.medtesthist.dao.exceptions.TestCaseDoesNotExistException;
import com.ge.med.medtesthist.model.TestCase;
import com.ge.med.medtesthist.model.views.TestCaseDTO;
import lombok.Setter;
import lombok.extern.java.Log;

import javax.persistence.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: kpolak
 * Date: 02/12/13
 * Time: 22:39
 * To change this template use File | Settings | File Templates.
 */
@Log
public class TestCaseDAO {

  @Setter
  EntityManager em;

  public TestCase findTestCase(String testCode) {
    Query q = em.createNamedQuery("TestCase.findByCode");
    q.setParameter("testCode", testCode);
    try {
      return (TestCase) q.getSingleResult();
    } catch (NoResultException nre) {
      return null;
    }
  }

  public List<TestCaseDTO> findTestCaseExecutions(String testCode) {
    TypedQuery<TestCaseDTO> q = em.createQuery("SELECT new com.ge.med.medtesthist.model.views.TestCaseDTO(" +
            " ptc.id, ptc.testCase.testCode, ptc.protocol.fileName, ptc.testResult, ptc.tester, ptc.executionDate," +
            " ptc.problemDescription, ptc.sprFlag, ptc.sprNumber, ptc.observedResults, ptc.protocol.testVersion.versionName, " +
            " ptc.sprImpact, ptc.skipRationale, ptc.testCase.userNote)" +
            " FROM ProtocolTestCase ptc WHERE ptc.testCase.testCode like :testCode",
        TestCaseDTO.class
    );
    q.setParameter("testCode", "%" + testCode);
    List<TestCaseDTO> testCases = q.getResultList();
    log.info("findTestCaseExecutions (" + testCode + "): " + testCases.size());
    return testCases;
  }

  /**
   * Stores TestCase into database
   *
   * @param testCase testCase to store
   * @throws TestCaseCannotBeCreatedException thrown when any DB exception occurs
   */
  public void storeTestCase(TestCase testCase) throws TestCaseCannotBeCreatedException, DBModificationException {
    TestCase tc = findTestCase(testCase.getTestCode());
    if (tc != null) {
      throw new TestCaseCannotBeCreatedException("Test case with code " + testCase.getTestCode() + " already exists " +
          "id DB!");
    }
    EntityTransaction tx = em.getTransaction();
    try {
      tx.begin();
      em.persist(testCase);
      tx.commit();
    } catch (Exception ex) {
      throw new DBModificationException(ex.getMessage());
    }
  }

  /**
   * Updates test case user note. User not is overwritten, however contains old value and concatenated new value.
   *
   * @param testCode TestCase code
   * @param userNote user note String
   * @throws TestCaseDoesNotExistException thrown when test case with given test code is not found in DB
   * @throws DBModificationException       thrown when other DB exception occurs
   */
  public void updateTestCaseUserNote(String testCode, String userNote)
      throws TestCaseDoesNotExistException, DBModificationException {
    TestCase tc = findTestCase(testCode);
    if (tc == null) {
      throw new TestCaseDoesNotExistException("Test case " + testCode + " does not exist in DB!");
    }

    tc.setUserNote(userNote);
    EntityTransaction tx = em.getTransaction();
    try {
      tx.begin();
      em.merge(tc);
      tx.commit();
      log.info("Test case " + testCode + " was updated with user note.");
    } catch (Exception ex) {
      throw new DBModificationException(ex.getMessage());
    }
  }

  public void removeTestCase(String testCode) throws TestCaseDoesNotExistException, DBModificationException {
    TestCase tc = findTestCase(testCode);
    if (tc == null) {
      throw new TestCaseDoesNotExistException("Test case " + testCode + " does not exist in DB!");
    }

    EntityTransaction tx = em.getTransaction();
    try {
      tx.begin();
      em.remove(tc);
      tx.commit();
      log.info("Test case " + testCode + " was removed.");
    } catch (Exception ex) {
      throw new DBModificationException(ex.getMessage());
    }
  }
}
