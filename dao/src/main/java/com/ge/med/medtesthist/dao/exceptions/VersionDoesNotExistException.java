package com.ge.med.medtesthist.dao.exceptions;

/**
 * TO BE DOCUMENTED
 * User: Krzysztof Polak
 * Date: 27/01/14
 * Time: 12:02
 */
public class VersionDoesNotExistException extends Exception {
  public VersionDoesNotExistException(String s) {
    super(s);
  }
}
