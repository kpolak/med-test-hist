package com.ge.med.medtesthist.dao.exceptions;

/**
 * TO BE DOCUMENTED
 * User: Krzysztof Polak
 * Date: 05/02/14
 * Time: 17:27
 */
public class ProtocolAlreadyExistsException extends Throwable {
  public ProtocolAlreadyExistsException(String s) {
    super(s);
  }
}
