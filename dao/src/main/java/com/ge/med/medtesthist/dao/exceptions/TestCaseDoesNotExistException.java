package com.ge.med.medtesthist.dao.exceptions;

/**
 * TO BE DOCUMENTED
 * User: Krzysztof Polak
 * Date: 31/01/14
 * Time: 16:43
 */
public class TestCaseDoesNotExistException extends Exception {
  public TestCaseDoesNotExistException(String s) {
    super(s);
  }
}
