package com.ge.med.medtesthist.dao;

import com.ge.med.medtesthist.dao.exceptions.DBModificationException;
import com.ge.med.medtesthist.dao.exceptions.VersionDoesNotExistException;
import com.ge.med.medtesthist.model.TestVersion;
import com.ge.med.medtesthist.model.utils.StringUtils;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

/**
 * User: Krzysztof Polak
 * Date: 04/12/13
 * Time: 15:20
 */
public class TestVersionDAO {

  EntityManager em;

  public TestVersion findTestVersion(String versionName) {
    Query q = em.createNamedQuery("TestVersion.findByVersionName");
    q.setParameter("versionName", versionName);
    try {
      return (TestVersion) q.getSingleResult();
    } catch (NoResultException nre) {
      return null;
    }
  }

  public void createVersionData(Map<String, String> sampleTestVersions, boolean onlyIfNoVersionExist)
      throws VersionDoesNotExistException, DBModificationException {
    if (onlyIfNoVersionExist) {
      List testVersions = em.createNamedQuery("TestVersion.findAll").getResultList();
      if (testVersions.size() > 0) {
        return;
      }
    }

    for (String version : sampleTestVersions.keySet()) {
      String parent = sampleTestVersions.get(version);

      TestVersion parentTestVersion = null;
      if (!StringUtils.isEmpty(parent)) {
        parentTestVersion = findTestVersion(parent);
        if (parentTestVersion == null) {
          throw new VersionDoesNotExistException("Version " + parent + " was not found in the database.");
        }
      }

      TestVersion tv = new TestVersion();
      tv.setVersionName(version);
      tv.setParentTestVersion(parentTestVersion);
      EntityTransaction tx = em.getTransaction();
      try {
        tx.begin();
        em.persist(tv);
        tx.commit();
      } catch (Exception ex) {
        throw new DBModificationException(ex.getMessage());
      }
    }
  }

  public List<TestVersion> getAllTestVersions() {
    TypedQuery<TestVersion> q = em.createNamedQuery("TestVersion.findAll", TestVersion.class);
    return q.getResultList();
  }

  public List<TestVersion> getTestVersionsForImportingProtocols() {
    TypedQuery<TestVersion> q = em.createNamedQuery("TestVersion.findValidForImport", TestVersion.class);
    return q.getResultList();
  }

  public void removeVersion(String versionName) throws VersionDoesNotExistException, DBModificationException {
    EntityTransaction tx = em.getTransaction();
    tx.begin();

    TestVersion testVersion = findTestVersion(versionName);
    if (testVersion == null) {
      throw new VersionDoesNotExistException("Version " + versionName + " cannot be deleted because it doesn't exist");
    }

    try {
      em.remove(testVersion);
      tx.commit();
    } catch (Exception ex) {
      throw new DBModificationException(ex.getMessage());
    }
  }

  public void createNewVersion(String newVersionName, String parentVersionName) throws DBModificationException {

    EntityTransaction tx = em.getTransaction();
    tx.begin();

    TestVersion parentTestVersion = findTestVersion(parentVersionName);

    TestVersion tv = new TestVersion();
    tv.setVersionName(newVersionName);
    tv.setParentTestVersion(parentTestVersion);

    try {
      em.persist(tv);
      tx.commit();
    } catch (javax.persistence.RollbackException ex) {
      throw new DBModificationException(ex.getCause().getMessage());
    }
  }

  public void setEm(EntityManager em) {
    this.em = em;
  }

}
