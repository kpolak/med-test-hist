package com.ge.med.medtesthist.dao;

import com.ge.med.medtesthist.dao.exceptions.DBModificationException;
import com.ge.med.medtesthist.dao.exceptions.ProtocolAlreadyExistsException;
import com.ge.med.medtesthist.dao.exceptions.TestCaseCannotBeCreatedException;
import com.ge.med.medtesthist.model.Protocol;
import com.ge.med.medtesthist.model.ProtocolTestCase;
import com.ge.med.medtesthist.model.TestCase;
import com.ge.med.medtesthist.model.TestVersion;
import com.ge.med.medtesthist.model.query.ProtocolQueryObject;
import com.ge.med.medtesthist.model.utils.StringUtils;
import com.ge.med.medtesthist.model.views.TestCaseDTO;
import com.ge.med.medtesthist.model.views.TestVersionDTO;
import lombok.extern.java.Log;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: kpolak
 * Date: 03/12/13
 * Time: 19:38
 * To change this template use File | Settings | File Templates.
 */
@Log
public class ProtocolDAO {

  EntityManager em;

  TestVersionDAO testVersionDAO;
  TestCaseDAO testCaseDAO;

  public Protocol findProtocol(BigDecimal protocolId) {
    Query q = em.createNamedQuery("Protocol.findById");
    q.setParameter("id", protocolId);
    try {
      return (Protocol) q.getSingleResult();
    } catch (NoResultException nre) {
      return null;
    }
  }

  public Protocol findProtocolByNameAndVersion(String fileName, String versionName) {
    Query q = em.createNamedQuery("Protocol.findByFileAndVersion");
    q.setParameter("fileName", fileName);
    q.setParameter("versionName", versionName);
    try {
      return (Protocol) q.getSingleResult();
    } catch (NoResultException nre) {
      return null;
    }
  }

  public Protocol updateTestVersion(Protocol protocol, String versionName) {
    TestVersion testVersion = testVersionDAO.findTestVersion(versionName);
    if (testVersion == null) {
      throw new IllegalStateException("Test round " + versionName + " not defined in DB.");
    }
    protocol.setTestVersion(testVersion);
    return protocol;
  }

  public Protocol updateTestCasesDefinition(Protocol protocol) {
    for (ProtocolTestCase ptc : protocol.getTestCases()) {
      TestCase testCase = ptc.getTestCase();
      String testCode = testCase.getTestCode();
      if (StringUtils.isEmpty(testCode)) {
        throw new IllegalStateException("Test code empty for test case in line " + ptc.getRowNbr());
      }

      TestCase tcInDB = testCaseDAO.findTestCase(testCode);
      if (tcInDB != null) {
        ptc.setTestCase(tcInDB);
      } else {
        try {
          testCaseDAO.storeTestCase(testCase);
        } catch (TestCaseCannotBeCreatedException e) {
          throw new IllegalStateException("Test case " + testCase.getTestCode() + " cannot be stored in the DB");
        } catch (DBModificationException e) {
          throw new IllegalStateException("Error saving test case " + testCase.getTestCode() + ". DB modification exc");
        }
        tcInDB = testCaseDAO.findTestCase(testCode);
        ptc.setTestCase(tcInDB);
      }
    }
    return protocol;
  }

  public List<TestVersionDTO> getTestVersionsWithProtocols(ProtocolQueryObject protocolQueryObject) {

    StringBuilder queryString = new StringBuilder("SELECT new com.ge.med.medtesthist.model.views.TestVersionDTO(" +
        " p.id, tv.versionName, tv.parentTestVersion.versionName, p.fileName, p.executionDate, p.testsNbr, p.testsPassedNbr, " +
        " p.testsSkippedNbr, p.testsFailedNbr, p.testsEmptyNbr, p.importDate, p.tester)" +
        " FROM TestVersion tv JOIN Protocol p WHERE tv.versionName = p.testVersion.versionName");

    queryString = applyConstraints(queryString, protocolQueryObject);
    TypedQuery<TestVersionDTO> q = em.createQuery(queryString.toString(), TestVersionDTO.class);
    applyParameters(q, protocolQueryObject);
    List<TestVersionDTO> testVersions = q.getResultList();
    System.out.println("findTestVersions " + testVersions.size());
    return testVersions;
  }

  private StringBuilder applyConstraints(StringBuilder queryString, ProtocolQueryObject protocolQueryObject) {
    if (protocolQueryObject == null) {
      return queryString;
    }

    if (!StringUtils.isEmpty(protocolQueryObject.getVersion())) {
      queryString = queryString.append(
          " and (tv.versionName = :versionName or tv.parentTestVersion.versionName = " + ":versionName)");
    }

    if (!StringUtils.isEmpty(protocolQueryObject.getProtocol())) {
      queryString = queryString.append(" and p.fileName like :fileName");
    }

    if (!StringUtils.isEmpty(protocolQueryObject.getTester())) {
      queryString = queryString.append(" and p.tester like :tester");
    }

    if (protocolQueryObject.getDateFrom() != null) {
      queryString = queryString.append(" and p.executionDate >= :dateFrom");
    }

    if (protocolQueryObject.getDateTo() != null) {
      queryString = queryString.append(" and p.executionDate <= :dateTo");
    }

    if (protocolQueryObject.isIncludePassed()) {
      queryString = queryString.append(" and  p.testsPassedNbr > 0");
    }

    if (protocolQueryObject.isIncludeSkipped()) {
      queryString = queryString.append(" and  p.testsSkippedNbr > 0");
    }

    if (protocolQueryObject.isIncludeFailed()) {
      queryString = queryString.append(" and  p.testsFailedNbr > 0");
    }

    if (protocolQueryObject.isIncludeEmpty()) {
      queryString = queryString.append(" and  p.testsEmptyNbr > 0");
    }

    return queryString;
  }

  private void applyParameters(TypedQuery<TestVersionDTO> q, ProtocolQueryObject protocolQueryObject) {
    if (protocolQueryObject == null) {
      return;
    }

    if (!StringUtils.isEmpty(protocolQueryObject.getVersion())) {
      q.setParameter("versionName", protocolQueryObject.getVersion());
    }

    if (!StringUtils.isEmpty(protocolQueryObject.getProtocol())) {
      q.setParameter("fileName", "%" + protocolQueryObject.getProtocol());
    }

    if (!StringUtils.isEmpty(protocolQueryObject.getTester())) {
      q.setParameter("tester", "%" + protocolQueryObject.getTester());
    }

    if (protocolQueryObject.getDateFrom() != null) {
      q.setParameter("dateFrom", protocolQueryObject.getDateFrom());
    }

    if (protocolQueryObject.getDateTo() != null) {
      q.setParameter("dateTo", protocolQueryObject.getDateTo());
    }
  }

  public void storeProtocol(Protocol protocol, boolean overwrite, boolean doCommit)
      throws DBModificationException, ProtocolAlreadyExistsException {
    Protocol p = findProtocolByNameAndVersion(protocol.getFileName(), protocol.getTestVersion().getVersionName());
    if (overwrite) {
      if (p != null) {
        removeProtocol(p.getId());
      }
    } else {
      if (p != null) {
        throw new ProtocolAlreadyExistsException(
            "Protocol " + p.getFileName() + " already exists in version " + p.getTestVersion().getVersionName());
      }
    }
    updateTestCasesDefinition(protocol);
    EntityTransaction tx = null;
    if (doCommit) {
      tx = em.getTransaction();
      tx.begin();
    }

    try {
      em.persist(protocol);
      if (doCommit) {
        tx.commit();
      }
    } catch (Exception ex) {
      throw new DBModificationException(ex.getMessage());
    }
  }

  public void removeProtocol(BigDecimal protocolId) {
    boolean newTransaction = false;
    EntityTransaction tx = em.getTransaction();
    // transaction can be active -
    // already started before while importing protocols with 'overwrite' selected
    if (!tx.isActive()) {
      tx.begin();
      newTransaction = true;
    }

    Protocol protocol = findProtocol(protocolId);
    if (protocol != null) {
//      testCaseDAO.removeTestCasesForProtocol(protocolId);
      em.remove(protocol);
      if (newTransaction) {
        tx.commit();
      }
    } else {
      if (newTransaction) {
        tx.rollback();
      }
    }
  }

  public List<String> processDuplicates(Protocol protocol, String testVersion) {
    List<String> errors = new ArrayList<>();
    Query q = em.createNamedQuery("Protocol.findByFileAndVersion");
    q.setParameter("fileName", protocol.getFileName());
    q.setParameter("versionName", testVersion);
    try {
      Protocol p = (Protocol) q.getSingleResult();
      if (p != null) {
        errors.add("Protocol already imported for this version on " + StringUtils.formatDate(protocol.getImportDate(), true));
      }
    } catch (NoResultException nre) {
      // no action needed
    }
    return errors;
  }

  public int getProtocolsCountForTestVersion(String versionName) {
    Query q = em.createNamedQuery("Protocol.countByVersionName");
    q.setParameter("versionName", versionName);
    Long ret = (Long) q.getSingleResult();
    return ret.intValue();
  }

  public void removeAllProtocolsInVersion(String versionName) {
    boolean newTransaction = false;
    EntityTransaction tx = em.getTransaction();
    // transaction can be active -
    // already started before while importing protocols with 'overwrite' selected
    if (!tx.isActive()) {
      tx.begin();
      newTransaction = true;
    }

    TypedQuery<Protocol> q = em.createNamedQuery("Protocol.findByVersionName", Protocol.class);
    q.setParameter("versionName", versionName);
    List<Protocol> protocols4Version = q.getResultList();

    protocols4Version.forEach(em::remove);

    if (newTransaction) {
      tx.commit();
    }
  }

  public List<TestCaseDTO> getTestCasesInProtocolByResult(String fileName, String versionName,
                                                          ProtocolTestCase.TestResultEnum testResult) {
    TypedQuery<TestCaseDTO> q = em.createQuery("SELECT new com.ge.med.medtesthist.model.views.TestCaseDTO(" +
                                                   " ptc.id, ptc.testCase.testCode, ptc.protocol.fileName, ptc.testResult, ptc.tester, ptc.executionDate," +
                                                   " ptc.problemDescription, ptc.sprFlag, ptc.sprNumber, ptc.observedResults, ptc.protocol.testVersion.versionName, " +
                                                   " ptc.sprImpact, ptc.skipRationale, ptc.testCase.userNote)" +
                                                   " FROM ProtocolTestCase ptc WHERE ptc.protocol.fileName like " +
                                                   ":fileName AND ptc.protocol.testVersion.versionName like " +
                                                   ":versionName AND ptc.testResult = :testResult", TestCaseDTO.class
    );
    q.setParameter("fileName", "%" + fileName);
    q.setParameter("versionName", versionName);
    q.setParameter("testResult", testResult);
    List<TestCaseDTO> testCases = q.getResultList();
    log.info(
        "getTestCasesInProtocolByResult (" + fileName + ", " + versionName + ", " + testResult + "): " + testCases.size());
    return testCases;
  }


  public void setEm(EntityManager em) {
    this.em = em;
  }

  public void setTestVersionDAO(TestVersionDAO testVersionDAO) {
    this.testVersionDAO = testVersionDAO;
  }

  public void setTestCaseDAO(TestCaseDAO testCaseDAO) {
    this.testCaseDAO = testCaseDAO;
  }
}
