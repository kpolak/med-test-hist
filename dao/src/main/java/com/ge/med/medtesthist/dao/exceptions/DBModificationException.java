package com.ge.med.medtesthist.dao.exceptions;

/**
 * TO BE DOCUMENTED
 * User: Krzysztof Polak
 * Date: 03/02/14
 * Time: 15:48
 */
public class DBModificationException extends Throwable {
  public DBModificationException(String message) {
    super(message);
  }
}
