package com.ge.med.medtesthist.gui;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.controlsfx.dialog.Dialogs;

/**
 * Created by kpolak on 26/12/13.
 */
public class ExceptionDlgTest extends Application {

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) {
    primaryStage.setTitle("Hello World!");
    Button btn = new Button();
    btn.setText("Say 'Hello World'");
    btn.setOnAction(new EventHandler<ActionEvent>() {

      @Override
      public void handle(ActionEvent event) {

        Exception ex = new Exception("This is an error");
        ex.setStackTrace(new StackTraceElement[]{});

        String str = "Line 1\n  Line 2\n  Line3";

        Dialogs.create()
            .owner(null)
            .title("Error")
            .masthead("Please provide valid path to file/folder!")
            .message(str)
            .lightweight()
            .showError();

      }
    });

    StackPane root = new StackPane();
    root.getChildren().add(btn);
    primaryStage.setScene(new Scene(root, 700, 450));
    primaryStage.show();
  }
}

