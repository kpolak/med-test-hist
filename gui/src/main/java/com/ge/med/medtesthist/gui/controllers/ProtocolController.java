package com.ge.med.medtesthist.gui.controllers;

import com.ge.med.medtesthist.model.ProtocolTestCase;
import com.ge.med.medtesthist.model.TestVersion;
import com.ge.med.medtesthist.model.query.ProtocolQueryObject;
import com.ge.med.medtesthist.model.utils.StringUtils;
import com.ge.med.medtesthist.model.views.ProtocolDTO;
import com.ge.med.medtesthist.model.views.TestCaseDTO;
import com.ge.med.medtesthist.model.views.TestVersionDTO;
import com.ge.med.medtesthist.services.ProtocolsService;
import com.ge.med.medtesthist.services.VersionsService;
import com.ge.med.medtesthist.gui.custom.ProtocolsTreeCellFactory;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.controlsfx.control.PopOver;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialogs;
import org.joda.time.DateTime;
import com.ge.med.medtesthist.gui.utils.DialogUtil;
import com.ge.med.medtesthist.gui.utils.TaskProgressUtil;

import java.util.List;

import static org.controlsfx.dialog.Dialog.Actions.NO;
import static org.controlsfx.dialog.Dialog.Actions.YES;

/**
 * Protocols tab controller.
 * User: Krzysztof Polak
 * Date: 12/12/13
 * Time: 11:07
 */
public class ProtocolController extends AbstractController {

  @FXML
  TreeTableView<TestVersionDTO> treeTableView;

  @FXML
  protected TreeTableColumn<TestVersionDTO, String> treeColVersion;
  @FXML
  protected TreeTableColumn<TestVersionDTO, String> treeColFileName;
  @FXML
  public TreeTableColumn treeColTestsNbr;
  @FXML
  public TreeTableColumn treeColTestsPassedNbr;
  @FXML
  public TreeTableColumn treeColTestsSkippedNbr;
  @FXML
  public TreeTableColumn treeColTestsFailedNbr;
  @FXML
  public TreeTableColumn<TestVersionDTO, String> treeColTester;
  @FXML
  public TreeTableColumn<TestVersionDTO, String> treeColExecutionDate;
  @FXML
  public TreeTableColumn<TestVersionDTO, String> treeColImportDate;
  @FXML
  private TreeTableColumn treeColTestsEmptyNbr;

  @FXML
  protected CheckBox chkPassedFX;
  @FXML
  protected CheckBox chkSkippedFX;
  @FXML
  protected CheckBox chkFailedFX;
  @FXML
  protected CheckBox chkEmptyFX;
//  @FXML
//  protected CheckBox chkDependedVersionsFX;

  @FXML
  TextField testerFX;
  @FXML
  TextField protocolFX;

  @FXML
  ComboBox<TestVersion> versionChoiceFX;

  @FXML
  public DatePicker critDateFrom;
  @FXML
  public DatePicker critDateTo;

  @FXML
  ProgressIndicator protProgressFX;
  private Stage stage;

  private VersionsService versionsService = new VersionsService();
  private ProtocolsService protocolsService = new ProtocolsService();

  public void initializeTab() {
    configureTreeTable();

    critDateFrom.setPromptText("-" + '\u221e');
    critDateTo.setPromptText("+" + '\u221e');

    protProgressFX.setVisible(false);
    TaskProgressUtil.hideProgress(protProgressFX);

    setupVersions();
  }

  /**
   * Configures TreeTable.
   */
  private void configureTreeTable() {

    TreeItem<TestVersionDTO> rootItem = createVersionsTreeDefinition();

    // rump-up protocols
    List<TestVersionDTO> testVersionsWithProtocols = protocolsService.getAllTestVersionsWithProtocols();
    for (TestVersionDTO testVersionsWithProtocol : testVersionsWithProtocols) {
      addProtocolTest(testVersionsWithProtocol);
    }

    rootItem.setExpanded(true);

    treeColVersion.setCellValueFactory(
            p -> {
                TestVersionDTO inv = p.getValue().getValue();
                return new ReadOnlyObjectWrapper<>(inv.toString());
            }
    );

    // Protocol column
    treeColFileName.setCellValueFactory(
            p -> {
                TestVersionDTO inv = p.getValue().getValue();
                return new ReadOnlyObjectWrapper<>(inv.getProtocolDTO().getFileName());
            }
    );

    // Nbr of tests column
    treeColTestsNbr.setCellValueFactory(
        new Callback<TreeTableColumn.CellDataFeatures<TestVersionDTO, String>, ObservableValue<String>>() {
          @Override
          public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<TestVersionDTO, String> p) {
            TestVersionDTO inv = p.getValue().getValue();
            return new ReadOnlyObjectWrapper(inv.getProtocolDTO().getTestsNbr());
          }
        }
    );
    treeColTestsNbr.setCellFactory(new ProtocolsTreeCellFactory(ProtocolTestCase.TestResultEnum.ALL, this));

    treeColTestsPassedNbr.setCellValueFactory(
        new Callback<TreeTableColumn.CellDataFeatures<TestVersionDTO, String>, ObservableValue<String>>() {
          @Override
          public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<TestVersionDTO, String> p) {
            TestVersionDTO inv = p.getValue().getValue();
            return new ReadOnlyObjectWrapper(inv.getProtocolDTO().getTestsPassedNbr());
          }
        }
    );
    treeColTestsPassedNbr.setCellFactory(new ProtocolsTreeCellFactory(ProtocolTestCase.TestResultEnum.PASSED, this));

    treeColTestsSkippedNbr.setCellValueFactory(
        new Callback<TreeTableColumn.CellDataFeatures<TestVersionDTO, String>, ObservableValue<String>>() {
          @Override
          public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<TestVersionDTO, String> p) {
            TestVersionDTO inv = p.getValue().getValue();
            return new ReadOnlyObjectWrapper(inv.getProtocolDTO().getTestsSkippedNbr());
          }
        }
    );
    treeColTestsSkippedNbr.setCellFactory(new ProtocolsTreeCellFactory(ProtocolTestCase.TestResultEnum.SKIPPED, this));

    treeColTestsFailedNbr.setCellValueFactory(
        new Callback<TreeTableColumn.CellDataFeatures<TestVersionDTO, String>, ObservableValue<String>>() {
          @Override
          public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<TestVersionDTO, String> p) {
            TestVersionDTO inv = p.getValue().getValue();
            return new ReadOnlyObjectWrapper(inv.getProtocolDTO().getTestsFailedNbr());
          }
        }
    );
    treeColTestsFailedNbr.setCellFactory(new ProtocolsTreeCellFactory(ProtocolTestCase.TestResultEnum.FAILED, this));

    treeColTestsEmptyNbr.setCellValueFactory(
        new Callback<TreeTableColumn.CellDataFeatures<TestVersionDTO, String>, ObservableValue<String>>() {
          @Override
          public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<TestVersionDTO, String> p) {
            TestVersionDTO inv = p.getValue().getValue();
            return new ReadOnlyObjectWrapper(inv.getProtocolDTO().getTestsEmptyNbr());
          }
        }
    );
    treeColTestsEmptyNbr.setCellFactory(new ProtocolsTreeCellFactory(ProtocolTestCase.TestResultEnum.EMPTY, this));

    treeColTester.setCellValueFactory(
            p -> {
                TestVersionDTO inv = p.getValue().getValue();
                return new ReadOnlyObjectWrapper<>(inv.getProtocolDTO().getTester());
            }
    );

    treeColExecutionDate.setCellValueFactory(
            p -> {
                TestVersionDTO inv = p.getValue().getValue();
                return new ReadOnlyObjectWrapper<>(inv.getProtocolDTO().getExecutionDateFormatted());
            }
    );

    treeColImportDate.setCellValueFactory(
            p -> {
                TestVersionDTO inv = p.getValue().getValue();
                return new ReadOnlyObjectWrapper<>(inv.getProtocolDTO().getImportDateFormatted());
            }
    );
  }

  private TreeItem<TestVersionDTO> createVersionsTreeDefinition() {
    TreeItem<TestVersionDTO> rootItem = new TreeItem<>(new TestVersionDTO("/", null, new ProtocolDTO()));
    treeTableView.setRoot(rootItem);

    // create tree definition based on versions entity
    List<TestVersion> testVersions = versionsService.getAllTestVersions();

      testVersions.stream().map(TestVersionDTO::createFromTestVersion).forEach(
              this::addTestVersions);
    return rootItem;
  }

  private void addProtocolTest(TestVersionDTO testVersionDTO) {
      TreeItem<TestVersionDTO> parent = findParentTreeItem(testVersionDTO.getVersionName(), treeTableView.getRoot());
    testVersionDTO.setVersionName("");
    TreeItem<TestVersionDTO> treeItem = new TreeItem<>(testVersionDTO);
      if (parent != null) {
          parent.getChildren().add(treeItem);
      }
  }

  private void addTestVersions(TestVersionDTO testVersionDTO) {
      TreeItem<TestVersionDTO> parent = findParentTreeItem(testVersionDTO.getParentVersionName(), treeTableView.getRoot());
    if (parent == null) {
      parent = treeTableView.getRoot();
    }
    TreeItem<TestVersionDTO> treeItem = new TreeItem<>(testVersionDTO);
    parent.getChildren().add(treeItem);
  }

  private TreeItem<TestVersionDTO> findParentTreeItem(String parentVersionName, TreeItem<TestVersionDTO> parent) {
    for (TreeItem<TestVersionDTO> treeItem : parent.getChildren()) {
      if (treeItem.getChildren().size() > 0) {
        TreeItem<TestVersionDTO> ti = findParentTreeItem(parentVersionName, treeItem);
        if (ti != null) {
          return ti;
        }
      }
      if (treeItem.getValue().getVersionName().equals(parentVersionName)) {
        return treeItem;
      }
    }
    return null;
  }

  private void setupVersions() {
    List<TestVersion> testVersions = versionsService.getAllTestVersions();
    testVersions.add(0, new TestVersion("ALL", null));
    ObservableList<TestVersion> observableList = FXCollections.observableList(testVersions);
    versionChoiceFX.getItems().addAll(observableList);
    versionChoiceFX.getSelectionModel().select(0);
  }

    public void handleSearchProtocols(@SuppressWarnings("UnusedParameters") ActionEvent actionEvent) {

    TaskProgressUtil.showProgress(protProgressFX);
    protProgressFX.setVisible(true);

    Task task = new Task<Void>() {
      @Override
      public Void call() {
        ProtocolQueryObject pqo = gatherConstraints();
        Platform.runLater(() -> {
          TreeItem<TestVersionDTO> rootItem = createVersionsTreeDefinition();
          List<TestVersionDTO> testVersionsWithProtocols = protocolsService.getTestVersionsWithProtocols(pqo);
          for (TestVersionDTO testVersionsWithProtocol : testVersionsWithProtocols) {
            addProtocolTest(testVersionsWithProtocol);
          }
          rootItem.setExpanded(true);
        });
        try {
          Thread.sleep(1000);
        } catch (InterruptedException ex) {
          ex.printStackTrace();
        }
        try {
          Thread.sleep(500);
          TaskProgressUtil.hideProgress(protProgressFX);
        } catch (InterruptedException ex) {
          ex.printStackTrace();
        }
        return null;
      }
    };

    //mwc.progressIndFX.progressProperty().bind(task.progressProperty());
    new Thread(task).start();
  }

  private ProtocolQueryObject gatherConstraints() {
    ProtocolQueryObject pqo = new ProtocolQueryObject();
    TestVersion testVersion = versionChoiceFX.getSelectionModel().getSelectedItem();
    if (testVersion != null && !testVersion.getVersionName().equals("ALL")) {
      pqo.setVersion(testVersion.getVersionName());
    }
    if (!StringUtils.isEmpty(protocolFX.getText())) {
      pqo.setProtocol(protocolFX.getText());
    }
    if (!StringUtils.isEmpty(testerFX.getText())) {
      pqo.setTester(testerFX.getText());
    }

    if (critDateFrom.getValue() != null) {
      pqo.setDateFrom(new DateTime(critDateFrom.getValue()));
    }
    if (critDateTo.getValue() != null) {
      pqo.setDateTo(new DateTime(critDateTo.getValue()));
    }

    if (chkPassedFX.isSelected()) {
      pqo.setIncludePassed(true);
    }
    if (chkSkippedFX.isSelected()) {
      pqo.setIncludeSkipped(true);
    }
    if (chkFailedFX.isSelected()) {
      pqo.setIncludeFailed(true);
    }
    if (chkEmptyFX.isSelected()) {
      pqo.setIncludeEmpty(true);
    }
    return pqo;
  }

  @FXML
  public void handleRemoveSelectedProtocol(@SuppressWarnings("UnusedParameters") ActionEvent actionEvent) {
      TreeItem<TestVersionDTO> ti = treeTableView.getSelectionModel().getSelectedItem();

    if (ti != null && ti.getValue() != null) {
      if (StringUtils.isEmpty(ti.getValue().getProtocolDTO().getFileName())) {
        DialogUtil.showErrorDialog("Only protocols can be deleted!");
        return;
      }

      String protocolFileName = ti.getValue().getProtocolDTO().getFileName();

      Action response = Dialogs.create().owner(stage).title("Confirmation").message(
          "Do you really want to delete protocol " + protocolFileName + "?").actions(YES,
          NO).lightweight().showConfirm();
      if (response != YES) {
        return;
      }

      protocolsService.removeProtocol(ti.getValue().getProtocolDTO().getProtocolId());

      handleSearchProtocols(null);
      DialogUtil.showInformationDialog("Protocol " + protocolFileName + " was successfuly deleted.");
    }
  }

  @FXML
  public void handleRemoveAllProtocolsInVersion(@SuppressWarnings("UnusedParameters") ActionEvent actionEvent) {
      TreeItem<TestVersionDTO> ti = treeTableView.getSelectionModel().getSelectedItem();

    // TODO change to optional
    // http://www.oracle.com/technetwork/articles/java/java8-optional-2175753.html
    if (ti != null && ti.getValue() != null) {
      if (!StringUtils.isEmpty(ti.getValue().getProtocolDTO().getFileName())) {
        DialogUtil.showErrorDialog("Please select a version first!");
        return;
      }

      String version = ti.getValue().getVersionName();

      Action response = Dialogs.create().owner(stage).title("Confirmation").message(
          "Do you really want to delete all protocols in version " + version + "?").actions(YES,
                                                                                            NO).lightweight().showConfirm();
      if (response != YES) {
        return;
      }

      protocolsService.removeAllProtocolsInVersion(version);

      handleSearchProtocols(null);
      DialogUtil.showInformationDialog("All protocols in version " + version + " were successfuly deleted.");
    }
  }


  PopOver pop;

  public void showDetailsPopup(TestVersionDTO testVersionDTO, String version,
                               ProtocolTestCase.TestResultEnum testResult,
                               double screenX, double screenY) {

    if (pop != null) {
      pop.hide();
    }

    VBox vbox = prepareDetailsPopupContent(testVersionDTO, version, testResult);
    pop = new PopOver(vbox);
    pop.setId("popover");
    pop.setArrowLocation(PopOver.ArrowLocation.LEFT_TOP);
    pop.setArrowIndent(-5);
    pop.setArrowSize(30);
    pop.setHideOnEscape(true);
    pop.setAutoHide(true);
    pop.setAutoFix(true);

    updateDetailTcs(testVersionDTO, version, testResult);

    pop.show(stage, screenX + 30, screenY - 30);
  }

  final ObservableList<TestCaseDTO> detailsPopoverTableContent = FXCollections.observableArrayList();
  TableView<TestCaseDTO> testCasesTableView = new TableView<>();


    private VBox prepareDetailsPopupContent(TestVersionDTO testVersionDTO, @SuppressWarnings("UnusedParameters") String version,
                                          ProtocolTestCase.TestResultEnum testResult) {

    String fileName = testVersionDTO.getProtocolDTO().getFileName();
    final Label label = new Label(fileName.substring(0, fileName.length() - 4) + " - " + testResult.toString());
    label.setFont(new Font("Arial", 14));

    testCasesTableView.setEditable(true);
    testCasesTableView.setPrefWidth(330);
    testCasesTableView.setPrefHeight(160);

        TableColumn<TestCaseDTO, String> testCaseCol = new TableColumn<>("Test case");
    testCaseCol.setPrefWidth(120);
        TableColumn<TestCaseDTO, String> resultCol = new TableColumn<>("Result");
        TableColumn<TestCaseDTO, String> testerCol = new TableColumn<>("Tester");
    testerCol.setPrefWidth(140);

    testCasesTableView.getColumns().clear();
    testCasesTableView.getColumns().addAll(testCaseCol, resultCol, testerCol);

        testCaseCol.setCellValueFactory(new PropertyValueFactory<>("testCode"));
        resultCol.setCellValueFactory(new PropertyValueFactory<>("testResult"));
        testerCol.setCellValueFactory(new PropertyValueFactory<>("tester"));

    testCasesTableView.setItems(detailsPopoverTableContent);

    TextArea resultsArea = new TextArea();
    resultsArea.setEditable(false);
    resultsArea.setPrefHeight(80);
    resultsArea.setPrefWidth(320);


    testCasesTableView.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {
      if (testCasesTableView.getSelectionModel().getSelectedItem() != null) {
          switch (newValue.getTestResult()) {
          case PASSED:
              resultsArea.setText(newValue.getObservedResults());
            break;
          case FAILED:
              resultsArea.setText("SPR " + newValue.getSprNumber() + ": " + newValue.getProblemDescription());
            break;
          case SKIPPED:
              resultsArea.setText(newValue.getSkipRationale());
            break;
        }
      }
    });

    VBox vbox = new VBox();
    vbox.setSpacing(5);
    vbox.setPadding(new Insets(10, 10, 10, 10));
    vbox.getChildren().addAll(label, testCasesTableView, resultsArea);
    return vbox;
  }

  private void updateDetailTcs(TestVersionDTO testVersionDTO, String version,
                                         ProtocolTestCase.TestResultEnum testResult) {
    String fileName = testVersionDTO.getProtocolDTO().getFileName();

    List<TestCaseDTO> tcs = protocolsService.getTestCasesInProtocolByResult(fileName, version, testResult);
    testCasesTableView.getItems().clear();
    Platform.runLater(() -> {
      testCasesTableView.getItems().addAll(tcs);
      testCasesTableView.autosize();
      testCasesTableView.layout();
    });
  }


    public void handleRefreshVersions(@SuppressWarnings("UnusedParameters") ActionEvent actionEvent) {
    setupVersions();
  }

  public void setStage(Stage stage) {
    this.stage = stage;
  }

}
