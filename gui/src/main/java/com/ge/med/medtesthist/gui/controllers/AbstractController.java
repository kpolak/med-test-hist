package com.ge.med.medtesthist.gui.controllers;

import com.ge.med.medtesthist.services.context.ContextData;

/**
 * TO BE DOCUMENTED
 * User: Krzysztof Polak
 * Date: 04/03/14
 * Time: 18:07
 */
public abstract class AbstractController implements ContextHandler {

  protected ContextData contextData;

  @Override
  public ContextData getContextObject() {
    return contextData;
  }

  @Override
  public void setContextObject(ContextData contextObject) {
    this.contextData = contextObject;
  }
}
