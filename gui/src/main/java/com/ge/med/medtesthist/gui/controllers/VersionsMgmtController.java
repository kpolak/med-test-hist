package com.ge.med.medtesthist.gui.controllers;

import com.ge.med.medtesthist.dao.exceptions.DBModificationException;
import com.ge.med.medtesthist.dao.exceptions.VersionDoesNotExistException;
import com.ge.med.medtesthist.model.TestVersion;
import com.ge.med.medtesthist.model.views.TestVersionDTO;
import com.ge.med.medtesthist.services.ProtocolsService;
import com.ge.med.medtesthist.services.VersionsService;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.stage.Stage;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialogs;
import com.ge.med.medtesthist.gui.utils.DialogUtil;

import java.util.List;

import static org.controlsfx.dialog.Dialog.Actions.NO;
import static org.controlsfx.dialog.Dialog.Actions.YES;

/**
 * TO BE DOCUMENTED
 * User: Krzysztof Polak
 * Date: 14/01/14
 * Time: 10:36
 */
public class VersionsMgmtController extends AbstractController {

  public static final String ROOT_LABEL = "/";

  @FXML
  TreeView<TestVersionDTO> treeView;
  private TreeItem<TestVersionDTO> rootItem;

  private Stage stage;

  private VersionsService versionsService = new VersionsService();
  private ProtocolsService protocolsService = new ProtocolsService();

  public void initializeTab() {
    createVersionsTreeDefinition();
  }

  private TreeItem<TestVersionDTO> createVersionsTreeDefinition() {
    rootItem = new TreeItem<>(new TestVersionDTO(ROOT_LABEL, null, null));
    treeView.setRoot(rootItem);

    // create tree definition based on versions entity
    List<TestVersion> testVersions = versionsService.getAllTestVersions();

    testVersions
        .stream()
        .map(p -> TestVersionDTO.createFromTestVersion(p))
        .forEach(testVersionDTO -> addTestVersions(testVersionDTO));

    expandChildren();

    return rootItem;
  }

  private void addTestVersions(TestVersionDTO testVersionDTO) {
    TreeItem parent = findParentTreeItem(testVersionDTO.getParentVersionName(), treeView.getRoot());
    if (parent == null) {
      parent = treeView.getRoot();
    }
    TreeItem<TestVersionDTO> treeItem = new TreeItem<>(testVersionDTO);
    parent.getChildren().add(treeItem);
  }

  private TreeItem<TestVersionDTO> findParentTreeItem(String parentVersionName, TreeItem<TestVersionDTO> parent) {
    for (TreeItem<TestVersionDTO> treeItem : parent.getChildren()) {
      if (treeItem.getChildren().size() > 0) {
        TreeItem<TestVersionDTO> ti = findParentTreeItem(parentVersionName, treeItem);
        if (ti != null) {
          return ti;
        }
      }
      if (treeItem.getValue().getVersionName().equals(parentVersionName)) {
        return treeItem;
      }
    }
    return null;
  }


  @FXML
  public void handleRemoveSelectedVersion(ActionEvent actionEvent) {
    TreeItem<TestVersionDTO> ti = treeView.getSelectionModel().getSelectedItem();

    if (ti == null || ti.getValue() == null || ti.getValue().getVersionName().equals(ROOT_LABEL)) {
      DialogUtil.showErrorDialog("You have to select some version first!");
      return;
    }

    if (ti.getChildren().size() > 0) {
      DialogUtil.showErrorDialog("You cannot delete version with children versions  - try to remove sub-versions first!");
      return;
    }

    TestVersionDTO testVersionDTO = ti.getValue();

    int protocolsInVersion = protocolsService.getProtocolsCountForTestVersion(testVersionDTO.getVersionName());

    if (protocolsInVersion > 0) {
      DialogUtil.showErrorDialog("You cannot delete version with imported protocols - try to remove protocols first!");
      return;
    }

    Action response = Dialogs.create()
        .owner(stage)
        .title("Confirmation")
        .message("Do you really want to delete version " + testVersionDTO.getVersionName() + "?")
        .actions(YES, NO)
        .lightweight()
        .showConfirm();
    if (response != YES) {
      return;
    }

    try {
      versionsService.removeVersion(testVersionDTO.getVersionName());
      createVersionsTreeDefinition();
    } catch (VersionDoesNotExistException | DBModificationException e) {
      e.printStackTrace();
      DialogUtil.showErrorDialog("Version cannot be deleted!\n" + e.getMessage());
    }
  }

  @FXML
  public void handleAddChildVersion(ActionEvent actionEvent) {
    // find parent version name
    // if it's not '/' and not empty then error
    // if it's '/' then add 2'nd level version
    // if it's empty, then add 1'st level version

    TreeItem<TestVersionDTO> ti = treeView.getSelectionModel().getSelectedItem();

    if (ti == null || ti.getValue() == null) {
      DialogUtil.showErrorDialog("You have to select some version first!");
      return;
    }

    TestVersionDTO selectedTestVersionDTO = ti.getValue();

    if (ti.getParent() != null && ti.getParent().getValue() != null
        && !ti.getParent().getValue().getVersionName().equals(ROOT_LABEL)) {
      DialogUtil.showErrorDialog("You can add child versions only on root level or 1st level!");
      return;
    }

    String parentVersionName = "";
    if (ti.getParent() != null) {
      parentVersionName = ti.getValue().getVersionName();
    }

//    Optional<String> newVersionName = Dialogs.create()
//        .owner(null)
//        .title("Enter new version name")
//        .message("New version name")
//        .lightweight()
//        .showTextInput(parentVersionName);

    String newVersionName = Dialogs.create()
        .owner(null)
        .title("Enter new version name")
        .message("New version name")
        .lightweight()
        .showTextInput(parentVersionName);


//    if (!newVersionName.isPresent() || newVersionName.get().length() < 5) {
//      DialogUtil.showErrorDialog("New version name cannot be empty and has to have at least 5 characters!");
//      return;
//    }

    if (newVersionName == null || newVersionName.length() < 5) {
      DialogUtil.showErrorDialog("New version name cannot be empty and has to have at least 5 characters!");
      return;
    }

    try {
      versionsService.createNewVersion(newVersionName, parentVersionName);
      createVersionsTreeDefinition();
    } catch (DBModificationException ex) {
      ex.printStackTrace();
      DialogUtil.showErrorDialog("New version was not created!\n" + ex.getMessage());
    }
  }


  public void expandChildren() {
    Thread thread = new Thread(new Task<Void>() {
      @Override
      protected Void call() throws Exception {
        Platform.runLater(() -> {
          for (int i = 0; i < rootItem.getChildren().size(); i++) {
            TreeItem<TestVersionDTO> current = rootItem.getChildren().get(i);
            if (!current.isLeaf()) {
              current.setExpanded(true);
            }
            current = null;
          }
          rootItem.setExpanded(true);
          System.gc();
        });
        return null;
      }
    });
    thread.setDaemon(true);
    thread.start();
  }


  public void setStage(Stage stage) {
    this.stage = stage;
  }
}
