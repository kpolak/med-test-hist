package com.ge.med.medtesthist.gui.utils;

import org.controlsfx.dialog.Dialogs;

/**
 * TO BE DOCUMENTED
 * User: Krzysztof Polak
 * Date: 16/01/14
 * Time: 18:25
 */
public class DialogUtil {

  public static void showErrorDialog(String errMessage) {
    Dialogs.create()
        .owner(null)
        .title("Error")
        .message(errMessage)
        .lightweight()
        .showError();
  }

  public static void showInformationDialog(String infoMessage) {
    Dialogs.create()
        .owner(null)
        .title("Information")
        .message(infoMessage)
        .lightweight()
        .showInformation();
  }

}
