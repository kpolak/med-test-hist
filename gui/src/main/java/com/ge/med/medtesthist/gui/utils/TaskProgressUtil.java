package com.ge.med.medtesthist.gui.utils;

import javafx.animation.FadeTransition;
import javafx.application.Platform;
import javafx.scene.control.ProgressIndicator;
import javafx.util.Duration;
import lombok.extern.java.Log;

/**
 * Progress showing/hiding util class.
 * User: Krzysztof Polak
 * Date: 20/12/13
 * Time: 10:45
 */
@Log
public class TaskProgressUtil {

  private static FadeTransition messageTransition = null;
  private static FadeTransition reverseTransition = null;

  public static void hideProgressSlowly(ProgressIndicator progressIndFX) {
    hideProgress(progressIndFX, 6000);

  }

  public static void hideProgress(ProgressIndicator progressIndFX) {
    hideProgress(progressIndFX, 600);
  }


  private static void hideProgress(ProgressIndicator progressIndFX, int hidingTime) {
    if (progressIndFX != null) {
      messageTransition = new FadeTransition(Duration.millis(hidingTime), progressIndFX);
      messageTransition.setFromValue(1.0);
      messageTransition.setToValue(0.0);
      messageTransition.setCycleCount(1);
      messageTransition.setOnFinished(event -> {
        progressIndFX.setVisible(false);
        messageTransition.stop();
        reverseTransition = null;
      });
      progressIndFX.setVisible(true);
      messageTransition.play();
    }
  }

  public static void showProgress(ProgressIndicator progressIndFX) {
    if (progressIndFX != null) {
      reverseTransition = new FadeTransition(Duration.millis(1000), progressIndFX);
      reverseTransition.setFromValue(0.0);
      reverseTransition.setToValue(1.0);
      reverseTransition.setOnFinished(event -> {
        progressIndFX.setVisible(true);
        reverseTransition.stop();
      });
      progressIndFX.setVisible(true);
      reverseTransition.play();
    }
  }

  public static void showAndUpdateProgress(ProgressIndicator progressIndicator, double value) {
    if (progressIndicator == null) {
      return;
    }
//    log.info("Import progress: " + value);
    Platform.runLater(() -> {
      progressIndicator.setProgress(value);
      progressIndicator.setVisible(true);
    });
  }

}
