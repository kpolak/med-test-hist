package com.ge.med.medtesthist.gui.controllers;

import com.ge.med.medtesthist.dao.exceptions.DBModificationException;
import com.ge.med.medtesthist.dao.exceptions.ProtocolAlreadyExistsException;
import com.ge.med.medtesthist.model.TestVersion;
import com.ge.med.medtesthist.model.utils.StringUtils;
import com.ge.med.medtesthist.reader.ReadingResults;
import com.ge.med.medtesthist.services.ProtocolReaderService;
import com.ge.med.medtesthist.services.ProtocolsService;
import com.ge.med.medtesthist.services.VersionsService;
import com.google.common.collect.Sets;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import lombok.extern.java.Log;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialogs;
import org.joda.time.DateTime;
import com.ge.med.medtesthist.gui.utils.DialogUtil;
import com.ge.med.medtesthist.gui.utils.TaskProgressUtil;

import java.io.File;
import java.util.*;

import static org.controlsfx.dialog.Dialog.Actions.NO;
import static org.controlsfx.dialog.Dialog.Actions.YES;

/**
 * Importing excel files/folders tab controller.
 * User: Krzysztof Polak
 * Date: 16/12/13
 * Time: 15:09
 */
@Log
public class ImportsController extends AbstractController {

  @FXML
  Button chooseDirBtnFX;
  @FXML
  Button refreshVerBtnFX;

  @FXML
  TextField chosenDirTextFX;

  @FXML
  ComboBox<TestVersion> versionChoiceFX;

  @FXML
  ToggleGroup toggleGroupFF;
  @FXML
  RadioButton radioFileFX;
  @FXML
  RadioButton radioOverwriteFX;

  @FXML
  TextArea outputConsoleFX;

  @FXML
  ProgressIndicator progressIndFX;

  private ProtocolReaderService protocolReaderService = new ProtocolReaderService();
  private ProtocolsService protocolsService = new ProtocolsService();
  private VersionsService versionsService = new VersionsService();

  public void initializeTab() {
    setupVersions();
    setupFileDirectoryGroup();
    refreshVerBtnFX.setTooltip(new Tooltip("Refresh versions"));
    progressIndFX.setVisible(false);
//    TaskProgressUtil.hideProgress(progressIndFX);
  }

  @FXML
  protected void handleChooseDir(@SuppressWarnings("UnusedParameters") ActionEvent event) {
    boolean fileMode = radioFileFX.isSelected();

    File selectedDirectory;
    File initialDirectory;
    if (StringUtils.isEmpty(chosenDirTextFX.getText())) {
      initialDirectory = new File(getContextObject().getImportDirectory());
    } else {
      initialDirectory = new File(chosenDirTextFX.getText());
    }
    if (fileMode) {
      FileChooser chooser = new FileChooser();
      chooser.setTitle("File to import");

      chooser.setInitialDirectory(initialDirectory);
      try {
        selectedDirectory = chooser.showOpenDialog(null);
      } catch (Exception ex) {
        chooser.setInitialDirectory(null);
        selectedDirectory = chooser.showOpenDialog(null);
      }

    } else {
      DirectoryChooser chooser = new DirectoryChooser();
      chooser.setTitle("Directory to import");

      chooser.setInitialDirectory(initialDirectory);
      try {
        selectedDirectory = chooser.showDialog(null);
      } catch (Exception ex) {
        chooser.setInitialDirectory(null);
        selectedDirectory = chooser.showDialog(null);
      }
    }

    if (selectedDirectory != null) {
      chosenDirTextFX.setText(selectedDirectory.getAbsolutePath());
    }
  }

  @FXML
  protected void handleImport(@SuppressWarnings("UnusedParameters") ActionEvent event) throws InterruptedException {
    TestVersion testVersion = versionChoiceFX.getSelectionModel().getSelectedItem();
    boolean fileMode = radioFileFX.isSelected();
    boolean overwrite = radioOverwriteFX.isSelected();
    String selectedFileFolderString = chosenDirTextFX.getText();

    if (!areInputFieldsCorrect(testVersion, selectedFileFolderString)) {
      return;
    }

    outputConsoleFX.appendText("\n--- IMPORT STARTED: " + StringUtils.formatDate(DateTime.now(), true) + "\n");

    List<ReadingResults> rrList = new ArrayList<>();

    if (fileMode) {
      ReadingResults rr = protocolReaderService.readSingleProtocol(selectedFileFolderString,
                                                                   testVersion.getVersionName());
      rrList.add(rr);
    } else {
      rrList = protocolReaderService.readProtocolsFromDirectory(selectedFileFolderString, testVersion.getVersionName(),
                                                                false);
    }

    if (!overwrite) {
      protocolsService.processDuplicates(rrList, testVersion.getVersionName());
    }

    List<ReadingResults> rrToStore = new ArrayList<>();

    TaskProgressUtil.showProgress(progressIndFX);

    int processedProtocolNo = 0;
    double processingPercentageStep = 1.0 / rrList.size();
    for (ReadingResults rr : rrList) {
      double processingPercentage = processingPercentageStep * processedProtocolNo;
      TaskProgressUtil.showAndUpdateProgress(progressIndFX, processingPercentage);
      Thread.sleep(100);

      ++processedProtocolNo;
      if (rr.getErrorsList().size() == 0 && rr.getWarningsList().size() == 0) {
        rrToStore.add(rr);
      } else if (rr.getWarningsList().size() > 0 && rr.getErrorsList().size() == 0) {
        Action response = Dialogs.create().owner(null).title("Confirmation").masthead(
            "Protocol " + rr.getProtocol().getFileName() + " has warnings. Do you want to import it anyway?").message(
            ReadingResults.getMessage(rr.getWarningsList())).actions(YES, NO).lightweight().showConfirm();
        if (response == YES) {
          rrToStore.add(rr);
        }
      } else {
        Dialogs.create().owner(null).title("Error").masthead(
            "Protocol " + rr.getProtocol().getFileName() + " cannot be imported. See errors below:").message(
            ReadingResults.getMessage(rr.getErrorsList())).lightweight().showError();
      }
    }

    try {
      protocolReaderService.storeReadProtocols(rrToStore, overwrite);
    } catch (DBModificationException e) {
      e.printStackTrace();
      DialogUtil.showErrorDialog("Protocol was not stored!\n" + e.getMessage());
    } catch (ProtocolAlreadyExistsException e) {
      e.printStackTrace();
      DialogUtil.showErrorDialog("Error while storing protocol!!\n" + e.getMessage());
    }

    TaskProgressUtil.showAndUpdateProgress(progressIndFX, 1);
    TaskProgressUtil.hideProgressSlowly(progressIndFX);

    DialogUtil.showInformationDialog(rrToStore.size() + " protocols have been imported successfully.");

    displayResults(rrList, rrToStore);
    outputConsoleFX.appendText("--- IMPORT FINISHED: " + StringUtils.formatDate(DateTime.now(),
                                                                                true) + ". Imported: " + rrToStore.size() + ", not imported: " + (rrList.size() - rrToStore.size()) + "\n");
  }

  private void displayResults(List<ReadingResults> rrList, List<ReadingResults> stored) {
    for (ReadingResults readingResults : stored) {
      outputConsoleFX.appendText(
          "  Imported protocol: " + readingResults.getProtocol().getFileName() + " with " + readingResults.getProtocol().getTestsNbr() + " test cases.\n");
    }

    Set<ReadingResults> allSet = new HashSet<>(rrList);
    Set<ReadingResults> storedSet = new HashSet<>(stored);

    Set<ReadingResults> notStoredSet = Sets.symmetricDifference(allSet, storedSet);
    for (ReadingResults readingResults : notStoredSet) {
      outputConsoleFX.appendText("    NOT imported protocol: " + readingResults.getProtocol().getFileName() + "\n");
    }
  }

  private boolean areInputFieldsCorrect(TestVersion testVersion, String selectedFileFolderString) {
    if (testVersion == null) {
      DialogUtil.showErrorDialog("Test version was not selected!");
      return false;
    }

    File selectedFileFolder = new File(selectedFileFolderString);
    if (!selectedFileFolder.exists()) {
      DialogUtil.showErrorDialog("Please provide valid path to file/folder!");
      return false;
    }
    return true;
  }

  private void setupVersions() {
    List<TestVersion> testVersions = versionsService.getTestVersionsForImportingProtocols();
    ObservableList<TestVersion> observableList = FXCollections.observableList(testVersions);
    versionChoiceFX.getItems().clear();
    versionChoiceFX.getItems().addAll(observableList);
  }

  private void setupFileDirectoryGroup() {
    toggleGroupFF.selectedToggleProperty().addListener((ov, old_toggle, new_toggle) -> {
      if (toggleGroupFF.getSelectedToggle() != null) {
        //String toggleLabel = ((RadioButton)toggleGroupFF.getSelectedToggle()).getText();
        chosenDirTextFX.setText("");
      }
    });
  }

  public void handleRefreshVersions(@SuppressWarnings("UnusedParameters") ActionEvent actionEvent) {
    setupVersions();
  }
}
