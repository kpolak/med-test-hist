package com.ge.med.medtesthist.gui;

import com.ge.med.medtesthist.gui.controllers.MainWindowController;
import com.ge.med.medtesthist.services.management.ResourceManager;
import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * Main GUI application window.
 */
public class GuiApp extends Application {

  public static String RESOURCES_PATH = "com/ge/med/medtesthist/gui/resources/";


  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) throws Exception {

    MainWindowController mwc = new MainWindowController();
    mwc.setContextObject(ResourceManager.getContextData());
    mwc.setupAndDisplayMainWindow(primaryStage);
  }

  public static Image getImage(String imageName) {
    return new Image(RESOURCES_PATH + "img/" + imageName);
  }
}
