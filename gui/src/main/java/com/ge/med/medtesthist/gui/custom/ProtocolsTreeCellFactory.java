package com.ge.med.medtesthist.gui.custom;

import com.ge.med.medtesthist.gui.controllers.ProtocolController;
import com.ge.med.medtesthist.model.ProtocolTestCase;
import com.ge.med.medtesthist.model.views.ProtocolDTO;
import com.ge.med.medtesthist.model.views.TestVersionDTO;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeTableCell;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableRow;
import javafx.util.Callback;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;


/**
 * Tree Cell Factory for Protocol TreeTable.
 * Add cell coloring and handling double click in some cells.
 * User: Krzysztof Polak
 * Date: 16/12/13
 * Time: 10:54
 */
public class ProtocolsTreeCellFactory implements Callback<TreeTableColumn, TreeTableCell> {

  private final ProtocolController protocolController;
  private ProtocolTestCase.TestResultEnum testResult;

  public ProtocolsTreeCellFactory(ProtocolTestCase.TestResultEnum testResult, ProtocolController protocolController) {
    this.testResult = testResult;
    this.protocolController = protocolController;
  }

  @Override
  public TreeTableCell call(TreeTableColumn p) {

    TreeTableCell cell = new TreeTableCell<TestVersionDTO, Object>() {

      @Override
      public void updateItem(Object item, boolean empty) {
        super.updateItem(item, empty);
        setText(empty ? null : getString());
        setGraphic(null);
        TreeTableRow currentRow = getTreeTableRow();
        TestVersionDTO testVersionDTO = currentRow == null ? null : (TestVersionDTO) currentRow.getItem();

        clearPriorityStyle();
        if (testVersionDTO != null) {
          setCustomStyle(testVersionDTO);
        }
      }

      private void clearPriorityStyle() {
        ObservableList<String> styleClasses = getStyleClass();
        styleClasses.remove("resultSkipped");
        styleClasses.remove("resultFailed");
        styleClasses.remove("resultEmpty");
        styleClasses.remove("resultAll");
      }

      private void setCustomStyle(TestVersionDTO testVersionDTO) {
        if (testVersionDTO != null && testVersionDTO.getProtocolDTO() != null) {
          ProtocolDTO protocolDTO = testVersionDTO.getProtocolDTO();
          switch (testResult) {
            case ALL:
              if (protocolDTO.getTestsNbr() != null && protocolDTO.getTestsNbr() != (protocolDTO.getTestsPassedNbr() + protocolDTO.getTestsSkippedNbr() +
                  protocolDTO.getTestsFailedNbr() + protocolDTO.getTestsEmptyNbr())) {
                getStyleClass().add("resultAll");
              }
            case SKIPPED:
              if (protocolDTO.getTestsSkippedNbr() != null && protocolDTO.getTestsSkippedNbr() > 0) {
                getStyleClass().add("resultSkipped");
              }
              break;
            case FAILED:
              if (protocolDTO.getTestsFailedNbr() != null && protocolDTO.getTestsFailedNbr() > 0) {
                getStyleClass().add("resultFailed");
              }
              break;
            case EMPTY:
              if (protocolDTO.getTestsEmptyNbr() != null && protocolDTO.getTestsEmptyNbr() > 0) {
                getStyleClass().add("resultEmpty");
              }
              break;
          }
        }
      }

      private String getString() {
        return getItem() == null ? "" : getItem().toString();
      }
    };

//    cell.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
//      @Override
//      public void handle(MouseEvent event) {
//        if (event.getClickCount() > 1) {
//          System.out.println("dbl click " + cell.getText());
//          TableCell c = (TableCell) event.getSource();
//          System.out.println("Cell text: " + c.getText());
//        }
//      }
//    });

    cell.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      public void handle(final MouseEvent mouseEvent) {
        if (mouseEvent.getClickCount() > 1 && testResult != ProtocolTestCase.TestResultEnum.ALL && cell.getText() != null && !cell.getText().equals(
            "0")) {
          TreeTableRow currentRow = cell.getTreeTableRow();

          TestVersionDTO parent = (TestVersionDTO) currentRow.getTreeItem().getParent().getValue();
          String version = parent.getVersionName();

          TestVersionDTO testVersionDTO = (TestVersionDTO) currentRow.getItem();
          protocolController.showDetailsPopup(testVersionDTO, version, testResult, mouseEvent.getScreenX(),
                                              mouseEvent.getScreenY());
          System.out.println("dbl click " + testResult + " " + cell.getText());
        }
      }
    });


    return cell;
  }


}
