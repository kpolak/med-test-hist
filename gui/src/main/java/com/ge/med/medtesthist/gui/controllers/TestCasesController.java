package com.ge.med.medtesthist.gui.controllers;

import com.ge.med.medtesthist.dao.exceptions.DBModificationException;
import com.ge.med.medtesthist.dao.exceptions.TestCaseDoesNotExistException;
import com.ge.med.medtesthist.model.utils.StringUtils;
import com.ge.med.medtesthist.model.views.TestCaseDTO;
import com.ge.med.medtesthist.services.TestCasesDataService;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import org.joda.time.DateTime;
import com.ge.med.medtesthist.gui.utils.DialogUtil;
import com.ge.med.medtesthist.gui.utils.TaskProgressUtil;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Test cases table controller
 * User: Krzysztof Polak
 * Date: 12/12/13
 * Time: 11:41
 */
public class TestCasesController extends AbstractController implements Initializable {

  @FXML
  TableView<TestCaseDTO> testCasesTableView;
  @FXML
  TableColumn<TestCaseDTO, String> colTestCode;
  @FXML
  TableColumn<TestCaseDTO, String> colTestVersion;
  @FXML
  TableColumn<TestCaseDTO, String> colFileName;
  @FXML
  TableColumn<TestCaseDTO, String> colTestResult;
  @FXML
  TableColumn<TestCaseDTO, String> colTester;
  @FXML
  TableColumn<TestCaseDTO, String> colExecutionDate;
  @FXML
  TableColumn<TestCaseDTO, String> colObservedResults;

  @FXML
  TextArea taObserverResults;
  @FXML
  TextArea taUserNotes;

  @FXML
  TextField newUserNote;

  @FXML
  CheckBox detIsSprChk;
  @FXML
  TextField detProblemDesc;
  @FXML
  TextField detSprImpact;
  @FXML
  TextField detSprNumber;
  @FXML
  TextField detSkipRationale;


  final ObservableList<TestCaseDTO> tableContent = FXCollections.observableArrayList();

  @Override
  public void initialize(URL url, ResourceBundle rb) {
    configureTestCasesTable();
  }

  protected void configureTestCasesTable() {
    colTestCode.setCellValueFactory(new PropertyValueFactory<>("testCode"));
    colTestVersion.setCellValueFactory(new PropertyValueFactory<>("testVersion"));
    colFileName.setCellValueFactory(new PropertyValueFactory<>("fileName"));
    colTestResult.setCellValueFactory(new PropertyValueFactory<>("testResult"));
    colTester.setCellValueFactory(new PropertyValueFactory<>("tester"));
    colExecutionDate.setCellValueFactory(new PropertyValueFactory<>("executionDateFormatted"));
    colObservedResults.setCellValueFactory(new PropertyValueFactory<>("observedResultsShort"));

    testCasesTableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
    testCasesTableView.setItems(tableContent);

    testCasesTableView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
//    testCasesTableView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
//      @Override
//      public void changed(ObservableValue observableValue, Object oldValue, Object newValue) {
//        if (testCasesTableView.getSelectionModel().getSelectedItem() != null) {
//          TestCaseDTO tc = (TestCaseDTO) newValue;
//          taObserverResults.setText(tc.getFileName());
//        }
//      }
//    });


    testCasesTableView.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {
      if (testCasesTableView.getSelectionModel().getSelectedItem() != null) {
        taObserverResults.setText(newValue.getObservedResults());
        if (newValue.getSprFlag() != null) {
          detIsSprChk.setSelected(newValue.getSprFlag());
        } else {
          detIsSprChk.setSelected(false);
        }
        detProblemDesc.setText(newValue.getProblemDescription());
        detSprImpact.setText(newValue.getSprImpact());
        detSprNumber.setText(newValue.getSprNumber());
        detSkipRationale.setText(newValue.getSkipRationale());
        taUserNotes.setText(newValue.getUserNote());
      }
    });

  }

  public void handleSearchTestCases(String testCode, MainWindowController mwc) {
    if (StringUtils.isEmpty(testCode)) {
      DialogUtil.showErrorDialog("Please enter test case code!");
      return;
    }

    testCasesTableView.getItems().clear();
    testCasesTableView.autosize();
    taObserverResults.setText("");
    detIsSprChk.setSelected(false);
    detProblemDesc.setText("");
    detSprImpact.setText("");
    detSprNumber.setText("");
    detSkipRationale.setText("");
    taUserNotes.setText("");

    TaskProgressUtil.showProgress(mwc.progressIndFX);
    mwc.progressIndFX.setVisible(true);

    Task task = new Task<Void>() {
      @Override
      public Void call() {
        TestCasesDataService tcds = new TestCasesDataService();
        List<TestCaseDTO> testCases = tcds.findTestCaseExecutions(testCode);
        if (testCases.size() == 0) {
          Platform.runLater(() -> DialogUtil.showInformationDialog("Test case " + testCode + " was not found in the database."));
        } else {
          try {
            Thread.sleep(2000);
          } catch (InterruptedException ex) {
            ex.printStackTrace();
          }
          // updateProgress(progressCounter, 100);
          Platform.runLater(() -> {
            testCasesTableView.getItems().addAll(testCases);
            testCasesTableView.autosize();
            testCasesTableView.layout();
          });
        }
        try {
          Thread.sleep(500);
          TaskProgressUtil.hideProgress(mwc.progressIndFX);
        } catch (InterruptedException ex) {
          ex.printStackTrace();
        }
        return null;
      }
    };

    //mwc.progressIndFX.progressProperty().bind(task.progressProperty());
    new Thread(task).start();
  }

  public void handleAddUserNote(ActionEvent actionEvent) {

    TestCaseDTO testCaseDTO = testCasesTableView.getSelectionModel().getSelectedItem();
    if (testCaseDTO == null) {
      DialogUtil.showErrorDialog("Please select test case.");
      return;
    }

    if (StringUtils.isEmpty(newUserNote.getText())) {
      DialogUtil.showErrorDialog("Note cannot be empty.");
      return;
    }
    String addedOnPrefix = "[" + StringUtils.formatDate(DateTime.now(), true) + "]: ";
    taUserNotes.setText(taUserNotes.getText() + (StringUtils.isEmpty(taUserNotes.getText()) ? "" : "\n") + addedOnPrefix + newUserNote.getText());
    newUserNote.setText("");
    TestCasesDataService tcds = new TestCasesDataService();
    try {
      tcds.updateTestCaseUserNote(testCaseDTO.getTestCode(), taUserNotes.getText());
    } catch (TestCaseDoesNotExistException | DBModificationException e) {
      DialogUtil.showErrorDialog(e.getMessage());
    }
  }
}

