package com.ge.med.medtesthist.gui.controllers;

import com.ge.med.medtesthist.services.context.ContextData;

/**
 * TO BE DOCUMENTED
 * User: Krzysztof Polak
 * Date: 04/03/14
 * Time: 18:04
 */
public interface ContextHandler {

  ContextData getContextObject();

  void setContextObject(ContextData contextObject);

}
