package com.ge.med.medtesthist.gui.controllers;

import com.ge.med.medtesthist.gui.GuiApp;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;

import java.io.IOException;

import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import com.ge.med.medtesthist.gui.utils.TaskProgressUtil;

/**
 * Main window FX controller.
 * User: Krzysztof Polak
 * Date: 10/12/13
 * Time: 14:16
 */
public class MainWindowController extends AbstractController {

  private Scene scene;
//  private Stage stage;

  @FXML
  protected TabPane tabPane;
  @FXML
  protected Tab tabTestCases;

  @FXML
  protected BorderPane testCases;
  @FXML
  protected TestCasesController testCasesController;

  @FXML
  protected BorderPane protocols;
  @FXML
  protected ProtocolController protocolsController;


  @FXML
  protected BorderPane versionsMgmt;
  @FXML
  protected VersionsMgmtController versionsMgmtController;

  @FXML
  protected VBox imports;
  @FXML
  protected ImportsController importsController;

  @FXML
  ProgressIndicator progressIndFX;
  @FXML
  Label labelVersionFX;
  @FXML
  TextField testCaseInput;

  private boolean tabProtocolsInitialized = false;
  private boolean tabVersionsInitialized = false;
  private boolean tabImportInitialized = false;

  public MainWindowController() throws Exception {
    // FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(GuiApp.RESOURCES_PATH + "fxml/MainWindow.fxml"));
    FXMLLoader fxmlLoader = new FXMLLoader(
        getClass().getResource("/com/ge/med/medtesthist/gui/resources/fxml/MainWindow.fxml"));
    fxmlLoader.setController(this);
    try {
      Parent parent = fxmlLoader.load();
      scene = new Scene(parent, 1150, 800);
    } catch (IOException ex) {
      System.out.println("Error displaying main window");
      throw new RuntimeException(ex);
    }
  }

  public void setupAndDisplayMainWindow(Stage stage) {
//    this.stage = stage;
    stage.setScene(scene);
    stage.getIcons().add(GuiApp.getImage("icon_bug.png"));
    stage.setTitle("Med Test Database");

    labelVersionFX.setText(
        "Version: " + getContextObject().getVersionNumber() + " (" + getContextObject().getVersionDate() + ")");

    // tabTestCases.setGraphic(new ImageView(GuiApp.getImage("tab_test.gif")));
    TaskProgressUtil.hideProgress(progressIndFX);
    Platform.runLater(testCaseInput::requestFocus);

    tabPane.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
      if (!tabProtocolsInitialized && newValue.getText().equals("Browse Protocols")) {
        protocolsController.initializeTab();
        protocolsController.setContextObject(getContextObject());
        protocolsController.setStage(stage);
        tabProtocolsInitialized = true;
        // Loading content on demand
        // Parent root = (Parent) fXMLLoader.load(this.getClass().getResource(newValue.getText() + ".fxml").openStream());
        //    newValue.setContent(root);
      } else if (!tabVersionsInitialized && newValue.getText().equals("Versions management")) {
        versionsMgmtController.initializeTab();
        versionsMgmtController.setContextObject(getContextObject());
        versionsMgmtController.setStage(stage);
        tabVersionsInitialized = true;
      } else if (!tabImportInitialized && newValue.getText().equals("Import Protocols")) {
        importsController.initializeTab();
        importsController.setContextObject(getContextObject());
        tabImportInitialized = true;
      }
    });

    stage.hide();
    stage.show();
  }

  @FXML
  protected void handleSearchTestCases(@SuppressWarnings("UnusedParameters") ActionEvent event) {
    testCasesController.handleSearchTestCases(testCaseInput.getText(), this);
  }
}