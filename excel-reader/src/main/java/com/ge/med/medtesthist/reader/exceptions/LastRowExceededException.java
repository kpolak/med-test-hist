package com.ge.med.medtesthist.reader.exceptions;

/**
 * Exception thrown when we reach the line over the last while reading excel rows.
 * User: Krzysztof Polak
 * Date: 20/11/13
 * Time: 15:31
 */
public class LastRowExceededException extends Exception {

}
