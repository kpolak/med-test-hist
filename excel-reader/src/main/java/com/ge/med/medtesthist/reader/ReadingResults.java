package com.ge.med.medtesthist.reader;

import com.ge.med.medtesthist.model.Protocol;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Results of reading single protocol from excel file.
 */
public class ReadingResults {

  @Getter
  @Setter
  Protocol protocol;

  @Getter
  List<String> warningsList = new ArrayList<>();

  @Getter
  List<String> errorsList = new ArrayList<>();

  public static String getMessage(List<String> aList) {
    String msg = "";
    for (String s : aList) {
      msg = msg + '\u2022' + " " + s + "\n";
    }
    return msg;
  }
}
