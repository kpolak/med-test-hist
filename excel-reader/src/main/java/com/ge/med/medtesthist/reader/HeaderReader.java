package com.ge.med.medtesthist.reader;

import com.ge.med.medtesthist.model.ConfigData;
import com.ge.med.medtesthist.model.ConfigHeader;
import com.ge.med.medtesthist.reader.exceptions.LastRowExceededException;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import java.util.*;

/**
 * Excel protocol header reader.
 * User: Krzysztof Polak
 * Date: 05/12/13
 * Time: 15:46
 */
public class HeaderReader {

  private static final int CONFIG_ITEM_DESCRIPTION_COL_NBR = 1;
  private static final int FIRST_CONFIG_ROW_AFTER_TC_NUMBER = 3;
  private int numberOfConfigs = 0;

  public List<ConfigHeader> readHeader(HSSFSheet sheet, int lastTestCaseRowNumber) {

    boolean readComplete = false;
    int rowNumber = lastTestCaseRowNumber + FIRST_CONFIG_ROW_AFTER_TC_NUMBER;
    int rowIndex = 0;

    List<ConfigHeader> configHeaders = null;

    while (!readComplete) {
      Row row = sheet.getRow(rowNumber);
      try {
        String[] rowData = readRowData(row);
        if (configHeaders == null) {
          configHeaders = new LinkedList<>();
          for (int i = 1; i < rowData.length; i++) {
            ConfigHeader ch = new ConfigHeader();
            ch.setName(rowData[i]);
            configHeaders.add(ch);
          }
        } else {
          for (int i = 1; i < rowData.length; i++) {
            ConfigData cd = new ConfigData();
            cd.setName(rowData[0]);
            cd.setValue(rowData[i]);
            cd.setIndex(rowIndex);
            cd.setConfigHeader(configHeaders.get(i - 1));
            configHeaders.get(i - 1).getConfigData().add(cd);
          }
        }
      } catch (LastRowExceededException ltce) {
        readComplete = true;
      }
      ++rowNumber;
      ++rowIndex;
    }

    return configHeaders;
  }

  private String[] readRowData(Row row) throws LastRowExceededException {
    if (isLastConfigLine(row)) {
      throw new LastRowExceededException();
    }

    if (numberOfConfigs == 0) {
      String[] configNames = readNumberOfConfigs(row);
      numberOfConfigs = configNames.length;
      return configNames;
    } else {
      String[] rowData = new String[numberOfConfigs];
      int configIdx = 0;
      for (int col = CONFIG_ITEM_DESCRIPTION_COL_NBR; col < CONFIG_ITEM_DESCRIPTION_COL_NBR + numberOfConfigs; col++) {
        rowData[configIdx] = readConfigCell(row, col);
        ++configIdx;
      }
      return rowData;
    }
  }

  private String[] readNumberOfConfigs(Row row) {
    int configCell = CONFIG_ITEM_DESCRIPTION_COL_NBR;
    String configHeader;
    List<String> configNames = new LinkedList<>();
    while ((configHeader = readConfigCell(row, configCell)) != null) {
      configNames.add(configHeader);
      configCell++;
    }

    String[] configs = new String[configNames.size()];
    for (int i = 0; i < configNames.size(); i++) {
      configs[i] = configNames.get(i);
    }

    return configs;
  }

  private String readConfigCell(Row row, int col) {
    Cell cell = row.getCell(col);
    try {
      switch (cell.getCellType()) {
        case Cell.CELL_TYPE_BOOLEAN:
          return "" + cell.getBooleanCellValue();
        case Cell.CELL_TYPE_NUMERIC:
          if (HSSFDateUtil.isCellDateFormatted(cell)) {
            return cell.getDateCellValue().toString();
          } else {
            return "" + cell.getNumericCellValue();
          }
        case Cell.CELL_TYPE_STRING:
          return cell.getStringCellValue().trim();
        case Cell.CELL_TYPE_BLANK:
          return null;
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw new IllegalStateException("Exception while reading excel data: " + e.toString());
    }
    throw new IllegalArgumentException("Cell type not recognized: " + cell.getCellType());
  }

  private boolean isLastConfigLine(Row row) {
    Cell cell = row.getCell(1);
    return cell.getCellType() == Cell.CELL_TYPE_BLANK;
  }
}
