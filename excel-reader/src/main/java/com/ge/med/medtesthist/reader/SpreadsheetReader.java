package com.ge.med.medtesthist.reader;

import com.ge.med.medtesthist.model.ConfigHeader;
import com.ge.med.medtesthist.model.Protocol;
import com.ge.med.medtesthist.model.ProtocolTestCase;
import com.ge.med.medtesthist.model.TestCase;
import com.ge.med.medtesthist.model.attributes.SimpleAttribute;
import com.ge.med.medtesthist.reader.exceptions.LastRowExceededException;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.joda.time.DateTime;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Spreadsheet reader.
 * User: Krzysztof Polak
 * Date: 20/11/13
 * Time: 15:26
 */
public class SpreadsheetReader {

  public static final int FIRST_TEST_CASES_ROW = 21;
  public static final String SHEET_TEST_CASES = "Test Cases";

  public ReadingResults readSpreadsheet(String fileNameWithPath) throws IOException {

    ReadingResults readingResults = new ReadingResults();

    File file = new File(fileNameWithPath);
    //FileInputStream fis = null;
    try (FileInputStream fis = new FileInputStream(file)) {
      //Get the workbook instance for XLS file

      HSSFWorkbook workbook = new HSSFWorkbook(fis);
      HSSFSheet sheet = workbook.getSheet(SHEET_TEST_CASES);

      int rowNumber = 0;
      boolean endOfTests = false;

      Protocol protocol = new Protocol();
      protocol.setFileName(file.getName());

      for (Row row : sheet) {
        if (endOfTests) {
          break;
        }

        ++rowNumber;
        if (rowNumber < FIRST_TEST_CASES_ROW) {
          continue;
        }

        try {
          ProtocolTestCase ptc = readRow(row);
          ptc.setRowNbr(rowNumber);
          ptc.setProtocol(protocol);
          protocol.addTestCase(ptc);
        } catch (LastRowExceededException e) {
          endOfTests = true;
        }
      }

      List<ConfigHeader> protocolHeaderData = new HeaderReader().readHeader(sheet, rowNumber);
      protocol.setHeaderData(protocolHeaderData);

      updateProtocolCounters(readingResults, protocol);
      readingResults.setProtocol(protocol);
    }
    return readingResults;
  }

  private void updateProtocolCounters(ReadingResults readingResults, Protocol protocol) {
    protocol.setTestsNbr(protocol.getTestCases().size());
    protocol.setTestsPassedNbr(protocol.countWithResultFilter(
        ptc -> ptc.getTestResult() != null && ptc.getTestResult().equals(ProtocolTestCase.TestResultEnum.PASSED)));
    protocol.setTestsSkippedNbr(protocol.countWithResultFilter(
        ptc -> ptc.getTestResult() != null && ptc.getTestResult().equals(ProtocolTestCase.TestResultEnum.SKIPPED)));
    protocol.setTestsFailedNbr(protocol.countWithResultFilter(
        ptc -> ptc.getTestResult() != null && ptc.getTestResult().equals(ProtocolTestCase.TestResultEnum.FAILED)));
    protocol.setTestsEmptyNbr(protocol.countWithResultFilter(
        ptc -> ptc.getTestResult() != null && ptc.getTestResult().equals(ProtocolTestCase.TestResultEnum.EMPTY)));
    // execution data is taken from first test case

    if (protocol.getTestsNbr() != protocol.getTestsPassedNbr() + protocol.getTestsSkippedNbr() + protocol.getTestsFailedNbr()) {
      readingResults.getWarningsList().add("Total number of test cases is different then the sum of passed+skipped+failed!");
    }

    DateTime executionDate = protocol.calculateExecutionDate();
    if (executionDate.equals(Protocol.UNKNOWN_EXECUTION_DATE)) {
      readingResults.getWarningsList().add("Execution date cell is empty!");
    }
    protocol.setExecutionDate(executionDate);

    String tester = protocol.extractTester();
    if (tester.equals(Protocol.UNKNOWN_TESTER)) {
      readingResults.getWarningsList().add("Tester cell is empty!");
    }
    protocol.setTester(tester);
  }

  private ProtocolTestCase readRow(Row row) throws LastRowExceededException {
    if (isLastTestCase(row)) {
      throw new LastRowExceededException();
    }

    Map<String, SimpleAttribute> attributesToRead = TestCase.getAttributesToRead();

    ProtocolTestCase protocolTestCase = new ProtocolTestCase();
    for (SimpleAttribute attribute : attributesToRead.values()) {
      Object o = readAttribute(attribute, row);
      if (o != null) {
        attribute.updateProtocolTestCase(protocolTestCase, o);
      }
    }
    return protocolTestCase;
  }

  private Object readAttribute(SimpleAttribute attribute, Row row) {
    Cell cell = row.getCell(attribute.getCellIndex());
    try {
      switch (cell.getCellType()) {
        case Cell.CELL_TYPE_BOOLEAN:
          return attribute.readAttribute(cell.getBooleanCellValue());
        case Cell.CELL_TYPE_NUMERIC:
          if (HSSFDateUtil.isCellDateFormatted(cell)) {
            //String dateFmt = cell.getCellStyle().getDataFormatString();
            return attribute.readAttribute(cell.getDateCellValue());
          } else {
            return attribute.readAttribute(cell.getNumericCellValue());
          }
        case Cell.CELL_TYPE_STRING:
          return attribute.readAttribute(cell.getStringCellValue().trim());
        case Cell.CELL_TYPE_BLANK:
          return null;
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw new IllegalStateException("Exception while reading excel data (attribute " + attribute.getAttributeName() + "): " + cell.getStringCellValue() + " in row " + row.getRowNum());
    }
    throw new IllegalArgumentException("Cell type not recognized: " + cell.getCellType() + "value: " + cell.getStringCellValue() + " in row " + row.getRowNum());
  }

  private boolean isLastTestCase(Row row) {
    Cell cell = row.getCell(0);
    return cell.getCellType() == Cell.CELL_TYPE_STRING && cell.getStringCellValue().equals("Last");
  }
}
