package com.ge.med.medtesthist.services.context;

import lombok.Getter;
import lombok.Setter;

/**
 * Context class containing all the parameters which can be set in the
 * external file medtest.properties
 * User: Krzysztof Polak
 * Date: 04/03/14
 * Time: 18:05
 */
public class ContextData {

  private String versionNumber;

  private String versionDate;

  private String importDirectory;

  private String jdbcDriver;

  private String jdbcUrl;

  private String dbUserName;

  private String dbPassword;

  private String jdbcLoggingLevel;

  public String getVersionNumber() {
    return versionNumber;
  }

  public void setVersionNumber(String versionNumber) {
    this.versionNumber = versionNumber;
  }

  public String getVersionDate() {
    return versionDate;
  }

  public void setVersionDate(String versionDate) {
    this.versionDate = versionDate;
  }

  public String getImportDirectory() {
    return importDirectory;
  }

  public void setImportDirectory(String importDirectory) {
    this.importDirectory = importDirectory;
  }

  public String getJdbcDriver() {
    return jdbcDriver;
  }

  public void setJdbcDriver(String jdbcDriver) {
    this.jdbcDriver = jdbcDriver;
  }

  public String getJdbcUrl() {
    return jdbcUrl;
  }

  public void setJdbcUrl(String jdbcUrl) {
    this.jdbcUrl = jdbcUrl;
  }

  public String getDbUserName() {
    return dbUserName;
  }

  public void setDbUserName(String dbUserName) {
    this.dbUserName = dbUserName;
  }

  public String getDbPassword() {
    return dbPassword;
  }

  public void setDbPassword(String dbPassword) {
    this.dbPassword = dbPassword;
  }

  public String getJdbcLoggingLevel() {
    return jdbcLoggingLevel;
  }

  public void setJdbcLoggingLevel(String jdbcLoggingLevel) {
    this.jdbcLoggingLevel = jdbcLoggingLevel;
  }
}
