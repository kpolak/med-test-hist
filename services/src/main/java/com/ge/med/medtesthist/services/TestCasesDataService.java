package com.ge.med.medtesthist.services;

import com.ge.med.medtesthist.dao.TestCaseDAO;
import com.ge.med.medtesthist.dao.exceptions.DBModificationException;
import com.ge.med.medtesthist.dao.exceptions.TestCaseDoesNotExistException;
import com.ge.med.medtesthist.model.views.TestCaseDTO;

import java.util.List;

/**
 * TO BE DOCUMENTED
 * User: Krzysztof Polak
 * Date: 10/12/13
 * Time: 14:47
 */
public class TestCasesDataService extends BaseService {

  private final static TestCaseDAO testCaseDAO = new TestCaseDAO();

  static {
    testCaseDAO.setEm(getEm());
  }

  public List<TestCaseDTO> findTestCaseExecutions(String testCode) {
    return testCaseDAO.findTestCaseExecutions(testCode);
  }

  public void updateTestCaseUserNote(String testCode, String userNote)
      throws TestCaseDoesNotExistException, DBModificationException {
    testCaseDAO.updateTestCaseUserNote(testCode, userNote);
  }
}
