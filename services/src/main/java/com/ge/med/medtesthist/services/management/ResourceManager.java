package com.ge.med.medtesthist.services.management;

import com.ge.med.medtesthist.services.context.ContextData;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Resource Manager. Provides DB connection to all services/dao classes.
 * User: Krzysztof Polak
 * Date: 17/01/14
 * Time: 09:14
 */
public class ResourceManager {

  private final static Map<String, String> properties = new HashMap<>();
  private final static ContextData contextData = buildContextData();

  static {
    properties.put("javax.persistence.jdbc.driver", contextData.getJdbcDriver());
    properties.put("javax.persistence.jdbc.url", contextData.getJdbcUrl());
    properties.put("javax.persistence.jdbc.user", contextData.getDbUserName());
    properties.put("javax.persistence.jdbc.password", contextData.getDbPassword());
    properties.put("eclipselink.logging.level", contextData.getJdbcLoggingLevel());
  }


  private final static EntityManagerFactory emf = Persistence.createEntityManagerFactory("medtestPU", properties);
  private final static EntityManager em = emf.createEntityManager();

  private static ContextData buildContextData() {
    Properties prop = new Properties();
    try {
      prop.load(new FileInputStream("medtest.properties"));
    } catch (FileNotFoundException e) {
      e.printStackTrace();
      throw new IllegalStateException("File 'medtest.properties' was not found!");
    } catch (IOException e) {
      e.printStackTrace();
      throw new IllegalStateException("Error while reading 'medtest.properties' file!");
    }

    ContextData contextData = new ContextData();
    contextData.setVersionNumber(prop.getProperty("versionName"));
    contextData.setVersionDate(prop.getProperty("versionDate"));
    contextData.setImportDirectory(prop.getProperty("importDirectory"));
    contextData.setJdbcDriver(prop.getProperty("jdbcDriver"));
    contextData.setJdbcUrl(prop.getProperty("jdbcUrl"));
    contextData.setDbUserName(prop.getProperty("dbUserName"));
    contextData.setDbPassword(prop.getProperty("dbPassword"));
    contextData.setJdbcLoggingLevel(prop.getProperty("jdbcLoggingLevel"));

    return contextData;
  }


  public static EntityManager getEntityManager() {
    return em;
  }

  public static ContextData getContextData() {
    return contextData;
  }
}
