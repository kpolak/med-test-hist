package com.ge.med.medtesthist.services;

import com.ge.med.medtesthist.services.management.ResourceManager;

import javax.persistence.EntityManager;

/**
 * TO BE DOCUMENTED
 * User: Krzysztof Polak
 * Date: 17/01/14§
 * Time: 09:18
 */
public class BaseService {

  protected static EntityManager getEm() {
    return ResourceManager.getEntityManager();
  }

}
