package com.ge.med.medtesthist.services;

import com.ge.med.medtesthist.dao.ProtocolDAO;
import com.ge.med.medtesthist.dao.TestVersionDAO;
import com.ge.med.medtesthist.dao.exceptions.DBModificationException;
import com.ge.med.medtesthist.dao.exceptions.VersionDoesNotExistException;
import com.ge.med.medtesthist.model.TestVersion;

import java.util.List;

/**
 * Created by kpolak on 15/12/13.
 */
public class VersionsService extends BaseService {

  private final static TestVersionDAO testVersionDAO = new TestVersionDAO();
  private final static ProtocolDAO protocolDAO = new ProtocolDAO();

  static {
    testVersionDAO.setEm(getEm());
    protocolDAO.setEm(getEm());
  }

  public List<TestVersion> getAllTestVersions() {
    List<TestVersion> testVersionList = testVersionDAO.getAllTestVersions();
    testVersionList
        .stream()
        .forEach(testVersion -> updateProtocolsCount(testVersion));
    return testVersionList;
  }

  public List<TestVersion> getTestVersionsForImportingProtocols() {
    return testVersionDAO.getTestVersionsForImportingProtocols();
  }

  private void updateProtocolsCount(TestVersion testVersion) {
    int protocolsCount = protocolDAO.getProtocolsCountForTestVersion(testVersion.getVersionName());
    testVersion.setProtocolsCount(protocolsCount);
  }

  public void removeVersion(String versionName) throws VersionDoesNotExistException, DBModificationException {
    testVersionDAO.removeVersion(versionName);
  }

  public void createNewVersion(String newVersionName, String parentVersionName) throws DBModificationException {
    testVersionDAO.createNewVersion(newVersionName, parentVersionName);
  }
}
