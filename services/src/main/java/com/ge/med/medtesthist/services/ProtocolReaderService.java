package com.ge.med.medtesthist.services;

import com.ge.med.medtesthist.dao.ProtocolDAO;
import com.ge.med.medtesthist.dao.TestCaseDAO;
import com.ge.med.medtesthist.dao.TestVersionDAO;
import com.ge.med.medtesthist.dao.exceptions.DBModificationException;
import com.ge.med.medtesthist.dao.exceptions.ProtocolAlreadyExistsException;
import com.ge.med.medtesthist.reader.ReadingResults;
import com.ge.med.medtesthist.reader.SpreadsheetReader;
import lombok.extern.java.Log;
import org.joda.time.DateTime;

import javax.persistence.EntityTransaction;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Business service for reading protocols.
 */
@Log
public class ProtocolReaderService extends BaseService {

  private final static TestVersionDAO testVersionDAO = new TestVersionDAO();
  private final static TestCaseDAO testCaseDAO = new TestCaseDAO();
  private final static ProtocolDAO protocolDAO = new ProtocolDAO();

  static {
    testVersionDAO.setEm(getEm());
    testCaseDAO.setEm(getEm());
    protocolDAO.setEm(getEm());
    protocolDAO.setTestVersionDAO(testVersionDAO);
    protocolDAO.setTestCaseDAO(testCaseDAO);
  }

  public ReadingResults readSingleProtocol(String fileName, String version) {
    ReadingResults readingResults = null;
    try {
      readingResults = readProtocol(fileName);
      if (readingResults.getProtocol() != null) {
        protocolDAO.updateTestVersion(readingResults.getProtocol(), version);
        protocolDAO.updateTestCasesDefinition(readingResults.getProtocol());
      } else {
        throw new IllegalStateException("Protocol is empty");
      }
    } catch (IOException e) {
      e.printStackTrace();
    }

    return readingResults;
  }

  public List<ReadingResults> readProtocolsFromDirectory(String directory, String version, boolean recursively) {

    List<ReadingResults> protocolList = new LinkedList<>();

    ReadingResults readingResults;

    File folder = new File(directory);

    if (folder.listFiles() == null) {
      return protocolList;
    }

    for (final File fileEntry : folder.listFiles()) {
      System.out.println("-----------------------");
      System.out.println("Processing entry: " + fileEntry.toString());
      if (fileEntry.isDirectory() && recursively) {
        // readProtocolsFromDirectory(fileEntry.getAbsolutePath(), version);
        throw new IllegalArgumentException("Recursive subfolders are not yet implemented!");
      } else if (!fileEntry.isDirectory() && fileEntry.getName().toLowerCase().endsWith("xls")) {
        //log.info("Loading " + fileIdx + "/" + allFilesCount + ": " + fileEntry.getAbsolutePath());
        try {
          readingResults = readProtocol(fileEntry.getAbsolutePath());
          if (readingResults.getProtocol() != null) {
            protocolDAO.updateTestVersion(readingResults.getProtocol(), version);
            protocolDAO.updateTestCasesDefinition(readingResults.getProtocol());
          }
        } catch (Exception e) {
          e.printStackTrace();
          return Collections.EMPTY_LIST;
        }

        if (readingResults.getProtocol() != null) {
          protocolList.add(readingResults);
        }
      }
    }
    return protocolList;
  }


  public void storeReadProtocols(List<ReadingResults> readingResults, boolean overwrite)
      throws DBModificationException, ProtocolAlreadyExistsException {
    EntityTransaction tx = getEm().getTransaction();
    tx.begin();

    for (ReadingResults rr : readingResults) {
      protocolDAO.storeProtocol(rr.getProtocol(), overwrite, false);
    }

    tx.commit();
  }

  private ReadingResults readProtocol(String fileEntry) throws IOException {
    SpreadsheetReader reader = new SpreadsheetReader();
    ReadingResults readingResults = reader.readSpreadsheet(fileEntry);
    if (readingResults.getProtocol() != null) {
      readingResults.getProtocol().setImportDate(DateTime.now());
    }
    return readingResults;
  }
}
