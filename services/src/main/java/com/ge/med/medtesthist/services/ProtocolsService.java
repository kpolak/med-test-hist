package com.ge.med.medtesthist.services;

import com.ge.med.medtesthist.dao.ProtocolDAO;
import com.ge.med.medtesthist.dao.TestCaseDAO;
import com.ge.med.medtesthist.dao.exceptions.DBModificationException;
import com.ge.med.medtesthist.dao.exceptions.ProtocolAlreadyExistsException;
import com.ge.med.medtesthist.model.Protocol;
import com.ge.med.medtesthist.model.ProtocolTestCase;
import com.ge.med.medtesthist.model.query.ProtocolQueryObject;
import com.ge.med.medtesthist.model.views.TestCaseDTO;
import com.ge.med.medtesthist.model.views.TestVersionDTO;
import com.ge.med.medtesthist.reader.ReadingResults;

import java.math.BigDecimal;
import java.util.List;

/**
 * Protocols services.
 * User: Krzysztof Polak
 * Date: 13/12/13
 * Time: 15:40
 */
public class ProtocolsService extends BaseService {

  private final static ProtocolDAO protocolDAO = new ProtocolDAO();
  private final static TestCaseDAO testCaseDAO = new TestCaseDAO();

  static {
    protocolDAO.setEm(getEm());
    testCaseDAO.setEm(getEm());
    protocolDAO.setTestCaseDAO(testCaseDAO);
  }

  public List<TestVersionDTO> getAllTestVersionsWithProtocols() {
    return protocolDAO.getTestVersionsWithProtocols(null);
  }

  public List<TestVersionDTO> getTestVersionsWithProtocols(ProtocolQueryObject protocolQueryObject) {
    return protocolDAO.getTestVersionsWithProtocols(protocolQueryObject);
  }

  public void removeProtocol(BigDecimal protocolId) {
    protocolDAO.removeProtocol(protocolId);
  }

  public void processDuplicates(List<ReadingResults> rrList, String testVersion) {
    for (ReadingResults readingResults : rrList) {
      List<String> errors = protocolDAO.processDuplicates(readingResults.getProtocol(), testVersion);
      readingResults.getErrorsList().addAll(errors);
    }
  }

  public int getProtocolsCountForTestVersion(String versionName) {
    return protocolDAO.getProtocolsCountForTestVersion(versionName);
  }

  // created for testing purposes
  public void storeProtocol(Protocol protocol, boolean overwrite)
      throws DBModificationException, ProtocolAlreadyExistsException {
    protocolDAO.storeProtocol(protocol, overwrite, false);
  }

  public List<TestCaseDTO> getTestCasesInProtocolByResult(String fileName, String versionName,
                                                          ProtocolTestCase.TestResultEnum testResult) {
    return protocolDAO.getTestCasesInProtocolByResult(fileName, versionName, testResult);
  }

  public void removeAllProtocolsInVersion(String version) {
    protocolDAO.removeAllProtocolsInVersion(version);
  }
}
