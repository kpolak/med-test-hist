package com.ge.med.medtesthist.model;

import com.ge.med.medtesthist.model.utils.StringUtils;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/**
 * Protocol entity
 * User: Krzysztof Polak
 * Date: 20/11/13
 * Time: 15:27
 */

@Entity
@Table(name = "PROTOCOLS", uniqueConstraints = {@UniqueConstraint(columnNames = {"FILE_NAME", "TEST_VERSION"})})
@NamedQueries(
    {@NamedQuery(name = "Protocol.findAll", query = "SELECT p FROM Protocol p"), @NamedQuery(name = "Protocol.findById",
                                                                                             query = "SELECT p FROM Protocol p WHERE p.id = :id"), @NamedQuery(
        name = "Protocol.findByFileAndVersion",
        query = "SELECT p FROM Protocol p " + "WHERE p.fileName = :fileName AND p.testVersion.versionName = :versionName"), @NamedQuery(
        name = "Protocol.findByVersionName",
        query = "SELECT p FROM Protocol p WHERE p.testVersion.versionName = :versionName"), @NamedQuery(
        name = "Protocol.countByVersionName",
        query = "SELECT count(p) FROM Protocol p WHERE p.testVersion.versionName = :versionName")})
public class Protocol {

  public final static DateTime UNKNOWN_EXECUTION_DATE = new DateTime(1990, 1, 1, 1, 1);
  public static final String UNKNOWN_TESTER = "unknown tester";

  @Id
  @GeneratedValue
  @Getter
  private BigDecimal id;

  @ManyToOne(optional = false, cascade = CascadeType.PERSIST)
  @JoinColumn(name = "TEST_VERSION")
  @Setter
  @Getter
  private TestVersion testVersion;

  @Column(name = "FILE_NAME")
  @Getter
  @Setter
  private String fileName;

  @Column(name = "GUI_LANGUAGE")
  @Getter
  @Setter
  private String guiLanguage;

  @Getter
  @Setter
  private String tester;

  @Column(name = "EXECUTION_DATE")
  @Getter
  @Setter
  @Convert(converter = com.ge.med.medtesthist.model.converters.JodaDateTimeConverter.class)
  private DateTime executionDate;

  @Column(name = "IMPORT_DATE")
  @Getter
  @Setter
  @Convert(converter = com.ge.med.medtesthist.model.converters.JodaDateTimeConverter.class)
  private DateTime importDate;

  @Column(name = "TESTS_NBR")
  @Getter
  @Setter
  private Integer testsNbr;

  @Column(name = "TESTS_PASSED_NBR")
  @Getter
  @Setter
  private Integer testsPassedNbr;

  @Column(name = "TESTS_SKIPPED_NBR")
  @Getter
  @Setter
  private Integer testsSkippedNbr;

  @Column(name = "TESTS_FAILED_NBR")
  @Getter
  @Setter
  private Integer testsFailedNbr;

  @Getter
  @Setter
  @Column(name = "TESTS_EMPTY_NBR")
  private Integer testsEmptyNbr;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "protocol", cascade = CascadeType.ALL)
  @Getter
  private List<ProtocolTestCase> testCases = new ArrayList<>();

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "protocol", cascade = CascadeType.ALL)
  @Getter
  private List<ConfigHeader> configHeaders = new ArrayList<>();

  public void addTestCase(ProtocolTestCase ptc) {
    testCases.add(ptc);
    ptc.setProtocol(this);
  }

  public void setHeaderData(List<ConfigHeader> configHeaders) {
    this.configHeaders.addAll(configHeaders);
    for (ConfigHeader configHeader : configHeaders) {
      configHeader.setProtocol(this);
    }
  }

  public int getNumberOfTestCases() {
    if (testCases == null) {
      return 0;
    }
    return testCases.size();
  }

  @Override
  public String toString() {
    return "Protocol{" +
        "id=" + id +
        ", testVersion=" + testVersion +
        ", fileName='" + fileName + '\'' +
        ", tester='" + tester + '\'' +
        ", testCases=" + testCases.size() +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Protocol protocol = (Protocol) o;

    if (fileName != null ? !fileName.equals(protocol.fileName) : protocol.fileName != null) {
      return false;
    }
    if (guiLanguage != null ? !guiLanguage.equals(protocol.guiLanguage) : protocol.guiLanguage != null) {
      return false;
    }
    if (id != null ? !id.equals(protocol.id) : protocol.id != null) {
      return false;
    }
    if (testVersion != null ? !testVersion.equals(protocol.testVersion) : protocol.testVersion != null) {
      return false;
    }
    if (tester != null ? !tester.equals(protocol.tester) : protocol.tester != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (testVersion != null ? testVersion.hashCode() : 0);
    result = 31 * result + (fileName != null ? fileName.hashCode() : 0);
    result = 31 * result + (guiLanguage != null ? guiLanguage.hashCode() : 0);
    result = 31 * result + (tester != null ? tester.hashCode() : 0);
    return result;
  }

  public Integer countWithResultFilter(Predicate<ProtocolTestCase> testCasePredicate) {
    int sum = 0;
    for (ProtocolTestCase t : getTestCases()) {
      if (testCasePredicate.test(t)) {
        sum++;
      }
    }
    return Integer.valueOf(sum);
  }

  public DateTime calculateExecutionDate() {
    for (ProtocolTestCase testCase : testCases) {
      if (testCase.getExecutionDate() != null) {
        return testCase.getExecutionDate();
      }
    }
    return UNKNOWN_EXECUTION_DATE;
  }

  public String extractTester() {
    for (ProtocolTestCase testCase : testCases) {
      if (!StringUtils.isEmpty(testCase.getTester())) {
        return testCase.getTester();
      }
    }
    return UNKNOWN_TESTER;
  }
}
