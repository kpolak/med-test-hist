package com.ge.med.medtesthist.model.attributes;

import com.ge.med.medtesthist.model.ProtocolTestCase;

/**
 * TO BE DOCUMENTED
 * User: Krzysztof Polak
 * Date: 21/11/13
 * Time: 11:35
 */
public class Precondition extends SimpleAttribute {

  public final static String ATTRIBUTE_NAME = "Precondition";

  @Override
  public int getCellIndex() {
    return 2;
  }

  @Override
  public Class getExcelClass() {
    return String.class;
  }

  @Override
  public String getAttributeName() {
    return ATTRIBUTE_NAME;
  }

  @Override
  public void updateProtocolTestCase(ProtocolTestCase protocolTestCase, Object o) {
    protocolTestCase.createTestCaseIfNeeded();
    protocolTestCase.getTestCase().setPrecondition((String) o);
  }
}
