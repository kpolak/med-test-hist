package com.ge.med.medtesthist.model.attributes;

import com.ge.med.medtesthist.model.ProtocolTestCase;

/**
 * TO BE DOCUMENTED
 * User: Krzysztof Polak
 * Date: 21/11/13
 * Time: 16:35
 */
public class ObservedResults extends SimpleAttribute {

  public final static String ATTRIBUTE_NAME = "Observed results";

  @Override
  public int getCellIndex() {
    return 12;
  }

  @Override
  public Class getExcelClass() {
    return String.class;
  }

  @Override
  public String getAttributeName() {
    return ATTRIBUTE_NAME;
  }

  @Override
  public void updateProtocolTestCase(ProtocolTestCase protocolTestCase, Object o) {
    protocolTestCase.setObservedResults((String) o);
  }
}
