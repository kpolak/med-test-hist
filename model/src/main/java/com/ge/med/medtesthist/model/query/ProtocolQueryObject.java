package com.ge.med.medtesthist.model.query;

import lombok.Data;
import org.joda.time.DateTime;

/**
 * TO BE DOCUMENTED
 * User: Krzysztof Polak
 * Date: 19/12/13
 * Time: 16:26
 */
@Data
public class ProtocolQueryObject {

  String version = null;
  // not used
  boolean includeChildren = true;
  String protocol = null;
  String tester = null;
  DateTime dateFrom = null;
  DateTime dateTo = null;
  boolean includePassed = false;
  boolean includeSkipped = false;
  boolean includeFailed = false;
  boolean includeEmpty = false;
}
