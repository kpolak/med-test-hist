package com.ge.med.medtesthist.model.attributes;

import com.ge.med.medtesthist.model.ProtocolTestCase;

/**
 * TO BE DOCUMENTED
 * User: Krzysztof Polak
 * Date: 21/11/13
 * Time: 13:37
 */
public class TestInput extends SimpleAttribute {

  public final static String ATTRIBUTE_NAME = "Test input";

  @Override
  public int getCellIndex() {
    return 3;
  }

  @Override
  public Class getExcelClass() {
    return String.class;
  }

  @Override
  public String getAttributeName() {
    return ATTRIBUTE_NAME;
  }

  @Override
  public void updateProtocolTestCase(ProtocolTestCase protocolTestCase, Object o) {
    protocolTestCase.createTestCaseIfNeeded();
    protocolTestCase.getTestCase().setTestInput((String) o);
  }
}
