package com.ge.med.medtesthist.model.attributes;

import com.ge.med.medtesthist.model.ProtocolTestCase;

/**
 * SkipRationale attribute definition class.
 * User: Krzysztof Polak
 * Date: 06/06/2014
 * Time: 16:58
 */
public class SkipRationale extends SimpleAttribute {

  public final static String ATTRIBUTE_NAME = "SkipRationale";

  @Override
  public int getCellIndex() {
    return 13;
  }

  @Override
  public Class getExcelClass() {
    return String.class;
  }

  @Override
  public String getAttributeName() {
    return ATTRIBUTE_NAME;
  }

  @Override
  public void updateProtocolTestCase(ProtocolTestCase protocolTestCase, Object o) {
    protocolTestCase.setSkipRationale((String) o);
  }
}