package com.ge.med.medtesthist.model.attributes;

import com.ge.med.medtesthist.model.ProtocolTestCase;
/**
 * TO BE DOCUMENTED
 * User: Krzysztof Polak
 * Date: 21/11/13
 * Time: 13:48
 */
public class TestOutput extends SimpleAttribute {

  public final static String ATTRIBUTE_NAME = "Test output";

  @Override
  public int getCellIndex() {
    return 4;
  }

  @Override
  public Class getExcelClass() {
    return String.class;
  }

  @Override
  public String getAttributeName() {
    return ATTRIBUTE_NAME;
  }

  @Override
  public void updateProtocolTestCase(ProtocolTestCase protocolTestCase, Object o) {
    protocolTestCase.createTestCaseIfNeeded();
    protocolTestCase.getTestCase().setTestOutput((String) o);
  }
}
