package com.ge.med.medtesthist.model.attributes;

import com.ge.med.medtesthist.model.ProtocolTestCase;
import lombok.EqualsAndHashCode;

/**
 * TO BE DOCUMENTED
 * User: Krzysztof Polak
 * Date: 06/06/2014
 * Time: 16:54
 */
@EqualsAndHashCode
public class SprNbr extends SimpleAttribute {

  public final static String ATTRIBUTE_NAME = "SprNbr";

  @Override
  public int getCellIndex() {
    return 9;
  }

  @Override
  public Class getExcelClass() {
    return String.class;
  }

  @Override
  public String getAttributeName() {
    return ATTRIBUTE_NAME;
  }

  @Override
  public void updateProtocolTestCase(ProtocolTestCase protocolTestCase, Object o) {
    protocolTestCase.setSprNumber((String) o);
  }
}
