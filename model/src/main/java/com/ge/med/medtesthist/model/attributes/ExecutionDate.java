package com.ge.med.medtesthist.model.attributes;

import com.ge.med.medtesthist.model.ProtocolTestCase;
import org.joda.time.DateTime;

import java.util.Date;

/**
 * TO BE DOCUMENTED
 * User: Krzysztof Polak
 * Date: 21/11/13
 * Time: 16:43
 */
public class ExecutionDate extends SimpleAttribute {

  public final static String ATTRIBUTE_NAME = "Execution date";

  @Override
  public int getCellIndex() {
    return 16;
  }

  @Override
  public Class getExcelClass() {
    return Date.class;
  }

  @Override
  public String getAttributeName() {
    return ATTRIBUTE_NAME;
  }

  @Override
  public Object readAttributeSpecific(Object value) {
    return new DateTime(((Date) value).getTime());
  }

  @Override
  public void updateProtocolTestCase(ProtocolTestCase protocolTestCase, Object o) {
    protocolTestCase.setExecutionDate((DateTime) o);
  }
}
