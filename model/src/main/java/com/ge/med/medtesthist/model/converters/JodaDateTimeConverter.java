package com.ge.med.medtesthist.model.converters;

import java.sql.Timestamp;

import org.eclipse.persistence.mappings.DatabaseMapping;
import org.eclipse.persistence.mappings.converters.Converter;
import org.eclipse.persistence.sessions.Session;
import org.joda.time.DateTime;

import javax.persistence.AttributeConverter;

/**
 * TO BE DOCUMENTED
 * User: Krzysztof Polak
 * Date: 02/12/13
 * Time: 11:34
 */
@javax.persistence.Converter(autoApply = true)
public class JodaDateTimeConverter implements AttributeConverter<DateTime, Timestamp> {

  private static final long serialVersionUID = 1L;

  @Override
  public Timestamp convertToDatabaseColumn(DateTime attribute) {
    if (attribute == null) {
      return null;
    } else {
      return new Timestamp(attribute.getMillis());
    }
  }

  @Override
  public DateTime convertToEntityAttribute(Timestamp dbData) {
    if (dbData == null) {
      return null;
    } else {
      return new DateTime(dbData.getTime());
    }
  }
}