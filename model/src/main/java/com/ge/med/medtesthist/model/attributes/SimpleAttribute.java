package com.ge.med.medtesthist.model.attributes;

import com.ge.med.medtesthist.model.ProtocolTestCase;
import com.ge.med.medtesthist.model.utils.StringUtils;

import java.text.ParseException;
import java.util.Date;

/**
 * TO BE DOCUMENTED
 * User: Krzysztof Polak
 * Date: 21/11/13
 * Time: 11:26
 */
public abstract class SimpleAttribute {

  public abstract int getCellIndex();

  public abstract Class getExcelClass();

  public final Object readAttribute(Object attributeValue) throws Exception {
    if (!(attributeValue.getClass().getName().equals(getExcelClass().getName()))) {

      if (getExcelClass().getName().equals(Date.class.getName()) && attributeValue.getClass().getName().equals(String.class.getName())) {
        try {
          Date transformedDate = StringUtils.transformToDate((String) attributeValue);
          return readAttributeSpecific(transformedDate);
        } catch (ParseException ex) {
          throw new Exception("Cannot read [" + getAttributeName() + "] attribute. Cannot parse [" + attributeValue + "] using " + StringUtils.DATE_PARSE_PATTERN + " format.");
        }
      } else if (getExcelClass().getName().equals(String.class.getName()) && attributeValue.getClass().equals(Double.class)) {
        return readAttributeSpecific(attributeValue.toString());
      }
      throw new Exception("Cannot read [" + getAttributeName() + "] attribute. Expected: " + getExcelClass() + ", encountered: " + attributeValue.getClass());
    }

    return readAttributeSpecific(attributeValue);
  }

  public abstract String getAttributeName();

  /**
   * This class should be overwritten for specific attributes.
   *
   * @param attributeValue attribute value
   */
  protected Object readAttributeSpecific(Object attributeValue) {
    return attributeValue;
  }

  public abstract void updateProtocolTestCase(ProtocolTestCase protocolTestCase, Object o);
}
