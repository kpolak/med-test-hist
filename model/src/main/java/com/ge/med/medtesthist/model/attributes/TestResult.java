package com.ge.med.medtesthist.model.attributes;

import com.ge.med.medtesthist.model.ProtocolTestCase;

/**
 * TO BE DOCUMENTED
 * User: Krzysztof Polak
 * Date: 21/11/13
 * Time: 13:49
 */
public class TestResult extends SimpleAttribute {

  public final static String ATTRIBUTE_NAME = "Test result";

  private final String PASSED = "Passed";
  private final String SKIPPED = "Skipped";
  private final String FAILED = "Failed";
  private final String EMPTY = "";

  @Override
  public int getCellIndex() {
    return 5;
  }

  @Override
  public Class getExcelClass() {
    return String.class;
  }

  @Override
  public String getAttributeName() {
    return ATTRIBUTE_NAME;
  }

  @Override
  public Object readAttributeSpecific(Object value) {
    if (value == null) {
      return ProtocolTestCase.TestResultEnum.EMPTY;
    }
    switch (value.toString()) {
      case PASSED:
        return ProtocolTestCase.TestResultEnum.PASSED;
      case SKIPPED:
        return ProtocolTestCase.TestResultEnum.SKIPPED;
      case FAILED:
        return ProtocolTestCase.TestResultEnum.FAILED;
      case EMPTY:
        return ProtocolTestCase.TestResultEnum.EMPTY;
    }
    throw new IllegalArgumentException("Field value [" + value.toString() + "] not valid for TestResult.");
  }

  @Override
  public void updateProtocolTestCase(ProtocolTestCase protocolTestCase, Object o) {
    protocolTestCase.setTestResult((ProtocolTestCase.TestResultEnum) o);
  }
}
