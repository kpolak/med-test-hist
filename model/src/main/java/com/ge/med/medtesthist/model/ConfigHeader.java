package com.ge.med.medtesthist.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Config header entity.
 * User: Krzysztof Polak
 * Date: 06/12/13
 * Time: 13:44
 */
@Entity
@Table(name = "CONFIG_HEADERS")
@NamedQuery(name = "ConfigHeader.findAll", query = "SELECT ch FROM ConfigHeader ch")
public class ConfigHeader {

  @Id
  @GeneratedValue
  @Getter
  private BigDecimal id;

  @ManyToOne(optional = false)
  @Getter
  @Setter
  Protocol protocol;

  @Getter
  @Setter
  String name;

  @OneToMany(mappedBy = "configHeader", cascade = CascadeType.ALL)
  @Getter
  private List<ConfigData> configData = new ArrayList<>();

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    ConfigHeader that = (ConfigHeader) o;

    if (id != null ? !id.equals(that.id) : that.id != null) return false;
    return !(name != null ? !name.equals(that.name) : that.name != null);

  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (name != null ? name.hashCode() : 0);
    return result;
  }
}
