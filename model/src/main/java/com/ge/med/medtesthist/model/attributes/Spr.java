package com.ge.med.medtesthist.model.attributes;

import com.ge.med.medtesthist.model.ProtocolTestCase;

/**
 * TO BE DOCUMENTED
 * User: Krzysztof Polak
 * Date: 21/11/13
 * Time: 14:03
 */
public class Spr extends SimpleAttribute {

  public final static String ATTRIBUTE_NAME = "Spr";

  private final String YES = "Yes";
  private final String NO = "No";

  @Override
  public int getCellIndex() {
    return 7;
  }

  @Override
  public Class getExcelClass() {
    return String.class;
  }

  @Override
  public String getAttributeName() {
    return ATTRIBUTE_NAME;
  }

  @Override
  public Object readAttributeSpecific(Object value) {
    switch (value.toString()) {
      case YES:
        return Boolean.TRUE;
      case NO:
        return Boolean.FALSE;
    }
    return null;
  }

  @Override
  public void updateProtocolTestCase(ProtocolTestCase protocolTestCase, Object o) {
    protocolTestCase.setSprFlag((Boolean) o);
  }

}
