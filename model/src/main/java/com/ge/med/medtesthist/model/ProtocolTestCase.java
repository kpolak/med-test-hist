package com.ge.med.medtesthist.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Test case execution entity.
 * User: Krzysztof Polak
 * Date: 02/12/13
 * Time: 15:50
 */
@Entity
@Table(name = "PROTOCOL_TEST_CASES", indexes = {@Index(columnList = "TEST_CODE")})
@NamedQuery(name = "ProtocolTestCase.findAll", query = "SELECT ptc FROM ProtocolTestCase ptc")
@EqualsAndHashCode
@ToString
public class ProtocolTestCase {

  public void createTestCaseIfNeeded() {
    if (this.testCase == null) {
      this.testCase = new TestCase();
    }
  }

  public enum TestResultEnum {
    ALL, PASSED, SKIPPED, FAILED, EMPTY
  }

  @Id
  @GeneratedValue
  @Getter
  private BigDecimal id;

  @Transient
  @Getter
  @Setter
  private String testCode;

  @Column(name = "ROW_NBR")
  @Getter
  @Setter
  private Integer rowNbr;

  @ManyToOne(optional = false)
  @Getter
  @Setter
  private Protocol protocol;

  @ManyToOne(optional = false, cascade = CascadeType.PERSIST)
  @JoinColumn(name = "TEST_CODE")
  @Getter
  @Setter
  private TestCase testCase;

  @Enumerated(EnumType.STRING)
  @Column(name = "TEST_RESULT", length = 100)
  @Getter
  @Setter
  private TestResultEnum testResult;

  @Getter
  @Setter
  private String tester;

  @Column(name = "EXECUTION_DATE")
  @Convert(converter = com.ge.med.medtesthist.model.converters.JodaDateTimeConverter.class)
  @Getter
  @Setter
  private DateTime executionDate;

  @Column(name = "PROBLEM_DESCRIPTION", length = 2000)
  @Getter
  @Setter
  private String problemDescription;

  @Column(name = "SPR_FLAG")
  @Getter
  @Setter
  private Boolean sprFlag;

  @Column(name = "SPR_IMPACT")
  @Getter
  @Setter
  private String sprImpact;

  @Column(name = "SPR_NUMBER")
  @Getter
  @Setter
  private String sprNumber;

  @Column(name = "OBSERVED_RESULTS", length = 5000)
  @Getter
  @Setter
  private String observedResults;

  @Column(name = "SKIP_RATIONALE")
  @Getter
  @Setter
  private String skipRationale;

}
