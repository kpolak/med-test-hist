package com.ge.med.medtesthist.model.attributes;

import com.ge.med.medtesthist.model.ProtocolTestCase;

/**
 * TesCode attribute.
 * User: Krzysztof Polak
 * Date: 21/11/13
 * Time: 11:25
 */
public class TestCode extends SimpleAttribute {

  public static String ATTRIBUTE_NAME = "Test code";

  @Override
  public int getCellIndex() {
    return 0;
  }

  @Override
  public Class getExcelClass() {
    return String.class;
  }

  @Override
  public String getAttributeName() {
    return ATTRIBUTE_NAME;
  }

  @Override
  public void updateProtocolTestCase(ProtocolTestCase protocolTestCase, Object o) {
    protocolTestCase.createTestCaseIfNeeded();
    protocolTestCase.getTestCase().setTestCode((String) o);
    protocolTestCase.getTestCase().setTestCode((String) o);
  }
}
