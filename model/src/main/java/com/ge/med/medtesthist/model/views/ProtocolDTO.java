package com.ge.med.medtesthist.model.views;

import com.ge.med.medtesthist.model.utils.StringUtils;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.math.BigDecimal;

/**
 * Created by kpolak on 11/12/13.
 */

@EqualsAndHashCode
@ToString
public class ProtocolDTO {

  @Getter
  @Setter
  private BigDecimal protocolId;

  @Getter
  @Setter
  private String fileName;

  @Getter
  @Setter
  private DateTime executionDate;

  @Getter
  @Setter
  private Integer testsNbr;

  @Getter
  @Setter
  private Integer testsPassedNbr;

  @Getter
  @Setter
  private Integer testsSkippedNbr;

  @Getter
  @Setter
  private Integer testsFailedNbr;

  @Getter
  @Setter
  private Integer testsEmptyNbr;

  @Getter
  @Setter
  private DateTime importDate;

  @Getter
  @Setter
  private String tester;

  public ProtocolDTO(String fileName) {
    this.fileName = fileName;
  }

  public ProtocolDTO(BigDecimal protocolId, String fileName, DateTime executionDate, Integer testsNbr, Integer testsPassedNbr, Integer testsSkippedNbr,
                     Integer testsFailedNbr, Integer testsEmptyNbr, DateTime importDate, String tester) {
    this.protocolId = protocolId;
    this.fileName = fileName;
    this.executionDate = executionDate;
    this.testsNbr = testsNbr;
    this.testsPassedNbr = testsPassedNbr;
    this.testsSkippedNbr = testsSkippedNbr;
    this.testsFailedNbr = testsFailedNbr;
    this.testsEmptyNbr = testsEmptyNbr;
    this.importDate = importDate;
    this.tester = tester;
  }

  public ProtocolDTO() {
  }

  public static ProtocolDTO emptyProtocolDTO() {
    return new ProtocolDTO(null, "", null, null, null, null, null, null, null, null);
  }

  public String getExecutionDateFormatted() {
    if (getExecutionDate() == null) {
      return "";
    }
    return StringUtils.formatDate(getExecutionDate(), false);
  }

  public String getImportDateFormatted() {
    if (getImportDate() == null) {
      return "";
    }
    return StringUtils.formatDate(getImportDate(), false);
  }
}
