package com.ge.med.medtesthist.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Config data entity - contains data for a single configuration
 * User: Krzysztof Polak
 * Date: 06/12/13
 * Time: 13:47
 */
@Entity
@Table(name = "CONFIG_DATA")
@NamedQuery(name = "ConfigData.findAll", query = "SELECT cd FROM ConfigData cd")
public class ConfigData {

  @Id
  @GeneratedValue
  @Getter
  private BigDecimal id;

  @ManyToOne(optional = false)
  @Getter
  @Setter
  ConfigHeader configHeader;

  @Getter
  @Setter
  Integer index;

  @Getter
  @Setter
  String name;

  @Getter
  @Setter
  String value;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    ConfigData that = (ConfigData) o;

    if (id != null ? !id.equals(that.id) : that.id != null) return false;
    if (index != null ? !index.equals(that.index) : that.index != null) return false;
    if (name != null ? !name.equals(that.name) : that.name != null) return false;
    return !(value != null ? !value.equals(that.value) : that.value != null);

  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (index != null ? index.hashCode() : 0);
    result = 31 * result + (name != null ? name.hashCode() : 0);
    result = 31 * result + (value != null ? value.hashCode() : 0);
    return result;
  }
}
