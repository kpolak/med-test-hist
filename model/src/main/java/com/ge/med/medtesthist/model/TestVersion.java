package com.ge.med.medtesthist.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: kpolak
 * Date: 02/12/13
 * Time: 20:02
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "TEST_VERSIONS")
@NamedQueries({
    @NamedQuery(name = "TestVersion.findByVersionName", query = "SELECT t FROM TestVersion t WHERE t.versionName = :versionName"),
    @NamedQuery(name = "TestVersion.findAll", query = "SELECT t FROM TestVersion t order by t.versionName"),
    @NamedQuery(name = "TestVersion.findValidForImport", query = "SELECT t FROM TestVersion t WHERE t.parentTestVersion is not null ORDER by t.versionName")
})
@EqualsAndHashCode
// , uniqueConstraints = @UniqueConstraint(columnNames = {"VERSION_NAME"})
public class TestVersion {

  @Id
  @Getter
  @Setter
  @Column(name = "VERSION_NAME")
  private String versionName;

  @ManyToOne(optional = false)
  @Getter
  @Setter
  @JoinColumn(name = "PARENT_VERSION")
  private TestVersion parentTestVersion;

  @Transient
  private int protocolsCount;

  public TestVersion() {
  }

  public TestVersion(String versionName, TestVersion parentTestVersion) {
    this.versionName = versionName;
    this.parentTestVersion = parentTestVersion;
  }

  @Override
  public String toString() {
    return versionName;
  }

  public void setProtocolsCount(int protocolsCount) {
    this.protocolsCount = protocolsCount;
  }

  public int getProtocolsCount() {
    return protocolsCount;
  }

//  @OneToMany(fetch = FetchType.LAZY, mappedBy = "testVersion", cascade = CascadeType.PERSIST)
//  @Getter
//  private List<Protocol> protocols = new ArrayList<>();


}
