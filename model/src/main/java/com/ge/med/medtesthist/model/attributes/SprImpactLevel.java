package com.ge.med.medtesthist.model.attributes;

import com.ge.med.medtesthist.model.ProtocolTestCase;

/**
 * TO BE DOCUMENTED
 * User: Krzysztof Polak
 * Date: 06/06/2014
 * Time: 17:41
 */
public class SprImpactLevel extends SimpleAttribute {

  public final static String ATTRIBUTE_NAME = "SprImpactLevel";

  @Override
  public int getCellIndex() {
    return 8;
  }

  @Override
  public Class getExcelClass() {
    return String.class;
  }

  @Override
  public String getAttributeName() {
    return ATTRIBUTE_NAME;
  }

  @Override
  public void updateProtocolTestCase(ProtocolTestCase protocolTestCase, Object o) {
    protocolTestCase.setSprImpact((String) o);
  }
}