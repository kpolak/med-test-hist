package com.ge.med.medtesthist.model;

/**
 * TO BE DOCUMENTED
 * User: Krzysztof Polak
 * Date: 20/11/13
 * Time: 15:55
 */

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Target(ElementType.FIELD)
@Inherited
@Retention(RetentionPolicy.RUNTIME)
public @interface SpreadsheetConfig {
  int columnIdx();
}