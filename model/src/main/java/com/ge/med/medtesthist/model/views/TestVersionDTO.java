package com.ge.med.medtesthist.model.views;

import com.ge.med.medtesthist.model.TestVersion;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.List;

/**
 * TO BE DOCUMENTED
 * User: Krzysztof Polak
 * Date: 13/12/13
 * Time: 14:59
 */
@EqualsAndHashCode
public class TestVersionDTO {

  @Getter
  @Setter
  private ProtocolDTO protocolDTO;

  @Getter
  @Setter
  private String versionName;

  @Getter
  @Setter
  private String parentVersionName;

  private int protocolsCount;

  public TestVersionDTO(String versionName, String parentVersionName, ProtocolDTO protocolDTO) {
    this.versionName = versionName;
    this.parentVersionName = parentVersionName;
    this.protocolDTO = protocolDTO;
  }

  public TestVersionDTO(BigDecimal protocolId, String versionName, String parentVersionName, String fileName, DateTime executionDate, Integer testsNbr,
                        Integer testsPassedNbr, Integer testsSkippedNbr, Integer testsFailedNbr, Integer testsEmptyNbr, DateTime importDate, String tester) {
    this.versionName = versionName;
    this.parentVersionName = parentVersionName;

    protocolDTO = new ProtocolDTO();
    protocolDTO.setProtocolId(protocolId);
    protocolDTO.setFileName(fileName);
    protocolDTO.setExecutionDate(executionDate);
    protocolDTO.setTestsNbr(testsNbr);
    protocolDTO.setTestsPassedNbr(testsPassedNbr);
    protocolDTO.setTestsSkippedNbr(testsSkippedNbr);
    protocolDTO.setTestsFailedNbr(testsFailedNbr);
    protocolDTO.setTestsEmptyNbr(testsEmptyNbr);
    protocolDTO.setImportDate(importDate);
    protocolDTO.setTester(tester);
  }


  public static TestVersionDTO createFromTestVersion(TestVersion testVersion) {
    TestVersionDTO testVersionDTO = new TestVersionDTO(testVersion.getVersionName(), testVersion.getParentTestVersion() != null ? testVersion.getParentTestVersion().getVersionName() : "", ProtocolDTO.emptyProtocolDTO());
    testVersionDTO.protocolsCount = testVersion.getProtocolsCount();
    return testVersionDTO;
  }

  @Override
  public String toString() {
    if (protocolsCount > 0) {
      return versionName + " [" + protocolsCount + "]";
    } else {
      return versionName;
    }
  }
}
