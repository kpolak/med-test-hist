package com.ge.med.medtesthist.model.views;

import com.ge.med.medtesthist.model.ProtocolTestCase;
import com.ge.med.medtesthist.model.utils.StringUtils;
import lombok.*;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.math.BigDecimal;

/**
 * Test Case DTO used to display Test Cases in GUI.
 * User: Krzysztof Polak
 * Date: 10/12/13
 * Time: 14:24
 */

@EqualsAndHashCode
@ToString
public class TestCaseDTO {

  @Getter
  @Setter
  private BigDecimal id;

  @Getter
  @Setter
  private String testCode;

  @Getter
  @Setter
  private String fileName;

  @Getter
  @Setter
  private ProtocolTestCase.TestResultEnum testResult;

  @Getter
  @Setter
  private String tester;

  @Getter
  @Setter
  private DateTime executionDate;

  @Getter
  @Setter
  private String problemDescription;

  @Getter
  @Setter
  private Boolean sprFlag;

  @Getter
  @Setter
  private String sprNumber;

  @Getter
  @Setter
  private String observedResults;

  @Getter
  @Setter
  private String testVersion;

  @Getter
  @Setter
  private String sprImpact;

  @Getter
  @Setter
  private String skipRationale;

  @Getter
  @Setter
  private String userNote;

  public TestCaseDTO() {
  }

  public TestCaseDTO(BigDecimal id, String testCode, String fileName, ProtocolTestCase.TestResultEnum testResult,
                     String tester, DateTime executionDate, String problemDescription, Boolean sprFlag,
                     String sprNumber, String observedResults, String testVersion, String sprImpact,
                     String skipRationale, String userNote) {
    this.id = id;
    this.testCode = testCode;
    this.fileName = fileName;
    this.testResult = testResult;
    this.tester = tester;
    this.executionDate = executionDate;
    this.problemDescription = problemDescription;
    this.sprFlag = sprFlag;
    this.sprNumber = sprNumber;
    this.observedResults = observedResults;
    this.testVersion = testVersion;
    this.sprImpact = sprImpact;
    this.skipRationale = skipRationale;
    this.userNote = userNote;
  }

  public String getExecutionDateFormatted() {
    return StringUtils.formatDate(getExecutionDate(), false);
  }

  public String getObservedResultsShort() {

    if (getObservedResults() == null) {
      return "";
    }

    int firstLine = getObservedResults().indexOf("\n");
    if (firstLine > 0) {
      int secondLine = getObservedResults().indexOf("\n", firstLine + 1);
      if (secondLine > 0) {
        return getObservedResults().substring(0, secondLine - 1).concat("...");
      }
    }
    if (getObservedResults().length() > 50) {
      return getObservedResults().substring(0, 50).concat("...");
    }
    return getObservedResults();
  }
}
